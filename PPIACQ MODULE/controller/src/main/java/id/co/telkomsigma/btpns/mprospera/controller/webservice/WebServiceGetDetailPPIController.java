package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.ppi.FamilyDataPPI;
import id.co.telkomsigma.btpns.mprospera.model.ppi.PPI;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.GetDetailPPIRequest;
import id.co.telkomsigma.btpns.mprospera.response.FamilyDataPPIResponse;
import id.co.telkomsigma.btpns.mprospera.response.GetDetailPPIResponse;
import id.co.telkomsigma.btpns.mprospera.service.PPIService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.WSValidationService;
import id.co.telkomsigma.btpns.mprospera.util.FieldValidationUtil;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webServiceGetDetailPPIController")
public class WebServiceGetDetailPPIController extends GenericController {

    JsonUtils jsonUtils = new JsonUtils();

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private PPIService ppiService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @RequestMapping(value = WebGuiConstant.PPI_GET_DETAIL_DATA_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    GetDetailPPIResponse getDetailAp3r(@RequestBody GetDetailPPIRequest request,
                                       @PathVariable("apkVersion") String apkVersion) throws Exception {

        final GetDetailPPIResponse responseCode = new GetDetailPPIResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        responseCode.setResponseMessage("SUKSES");
        responseCode.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        try {
            log.info("getDetailPPI INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                log.debug("START MAPPING DATA PPI");
                PPI ppi = ppiService.findPPIbyLocalId(request.getPpiId());
                if (ppi != null) {
                    responseCode.setJumlahAnggotaKeluarga(FieldValidationUtil.
                            integerValueResponseValidation(ppi.getFamilyNumber()));
                    responseCode.setKeluargaBersekolah(FieldValidationUtil.charValueResponseValidation(ppi.getFamilyInSchool()));
                    responseCode.setPendidikanKepalaKeluarga(ppi.getWifeEducation());
                    responseCode.setPekerjaanKepalaKeluarga(ppi.getHusbandJob());
                    responseCode.setJenisLantai(FieldValidationUtil.charValueResponseValidation(ppi.getFloorType()));
                    responseCode.setJenisWC(ppi.getToiletType());
                    responseCode.setBahanBakarUtama(FieldValidationUtil.charValueResponseValidation(ppi.getMainGas()));
                    responseCode.setTabungGas(FieldValidationUtil.charValueResponseValidation(ppi.getHasGasTube()));
                    responseCode.setKulkas(FieldValidationUtil.charValueResponseValidation(ppi.getHasRefrigerator()));
                    responseCode.setSepedaMotor(FieldValidationUtil.charValueResponseValidation(ppi.getHasMotorcycle()));
                    List<FamilyDataPPIResponse> familyDataPPIResponseList = new ArrayList<>();
                    List<FamilyDataPPI> familyDataPPIList = ppiService.findFamilyDataByPPIId(ppi.getPpiId());
                    for (FamilyDataPPI familyDataPPI : familyDataPPIList) {
                        FamilyDataPPIResponse familyDataPPIResponse = new FamilyDataPPIResponse();
                        familyDataPPIResponse.setFamilyId(familyDataPPI.getFamilyDataPpiLocalId());
                        familyDataPPIResponse.setName(familyDataPPI.getName());
                        familyDataPPIResponse.setFamilyStatus(familyDataPPI.getFamilyStatus());
                        familyDataPPIResponse.setDifferentBusiness(FieldValidationUtil.
                                charValueResponseValidation(familyDataPPI.getHasDifferentBusiness()));
                        familyDataPPIResponse.setAge(FieldValidationUtil.integerValueResponseValidation(familyDataPPI.getAge()));
                        familyDataPPIResponse.setEducationStatus(FieldValidationUtil.
                                charValueResponseValidation(familyDataPPI.getStillInSchool()));
                        familyDataPPIResponseList.add(familyDataPPIResponse);
                    }
                    responseCode.setFamilyList(familyDataPPIResponseList);
                }
                log.debug("FINISH MAPPING DATA DETAIL PPI");
            }
        } catch (Exception e) {
            log.error("getDetailPPI error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("getDetailAp3r error: " + e.getMessage());
        } finally {
            try {
                log.debug("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_AP3RDetail);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("getDetailPPI RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("getDetailPPI saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }

        }
        return responseCode;
    }

}