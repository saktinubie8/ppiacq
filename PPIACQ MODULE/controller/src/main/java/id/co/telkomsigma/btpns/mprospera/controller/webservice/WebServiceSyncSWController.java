package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.WismaPOJO;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.*;
import id.co.telkomsigma.btpns.mprospera.request.SyncSWRequest;
import id.co.telkomsigma.btpns.mprospera.response.SyncSWResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.FieldValidationUtil;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.PercentageSynchronizer;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webServiceSyncSWController")
public class WebServiceSyncSWController extends GenericController {

    JsonUtils jsonUtils = new JsonUtils();

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private SWService swService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private AP3RService ap3RService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private UserService userService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    // SINKRONISASI SW
    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_SW_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SyncSWResponse doList(@RequestBody final SyncSWRequest request,
                          @PathVariable("apkVersion") String apkVersion) throws Exception {

        SimpleDateFormat formatDateTime = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        String countData = request.getGetCountData();
        String page = request.getPage();
        String startLookupDate = request.getStartLookupDate();
        String endLookupDate = request.getEndLookupDate();
        final SyncSWResponse responseCode = new SyncSWResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        responseCode.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
        try {
            log.info("syncSW INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(), request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                log.debug("Validation success, get Survey Wawancara data");
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                final Page<SurveyWawancara> swPage = swService.getSWByUser(request.getUsername(), page, countData,
                        startLookupDate, endLookupDate);
                log.debug("Finishing get Survey Wawancara data");

                final List<SWPOJO> swResponses = new ArrayList<SWPOJO>();
                for (final SurveyWawancara sw : swPage) {
                    SWPOJO swPojo = new SWPOJO();
                    swPojo.setSwId(FieldValidationUtil.stringRequestValueValidation(sw.getLocalId()));
                    swPojo.setSwPPI(FieldValidationUtil.booleanResponseValueValidation(sw.getSwPPI()));
                    swPojo.setHasPPI(FieldValidationUtil.booleanResponseValueValidation(sw.getHasPPI()));
                    if (sw.getNasabahPotensial() != null) {
                        if (sw.getNasabahPotensial().equals(true)) {
                            swPojo.setIsNasabahPotensial("true");
                        } else {
                            swPojo.setIsNasabahPotensial("false");
                        }
                    } else {
                        swPojo.setIsNasabahPotensial("false");
                    }
                    swPojo.setDayLight(sw.getTime());
                    swPojo.setSwLocation(sw.getSwLocation());
                    // get wisma SW
                    WismaPOJO wisma = new WismaPOJO();
                    Location wismaSw = areaService.findLocationById(sw.getWismaId());
                    if (wismaSw != null) {
                        wisma.setOfficeId(wismaSw.getLocationId());
                        wisma.setOfficeCode(wismaSw.getLocationCode());
                        wisma.setOfficeName(wismaSw.getName());
                        swPojo.setWisma(wisma);
                    }
                    swPojo.setCustomerType(
                            FieldValidationUtil.charValueResponseValidation(sw.getCustomerRegistrationType()));
                    if (sw.getSurveyDate() != null)
                        swPojo.setSurveyDate(formatDateTime.format(sw.getSurveyDate()));
                    if (sw.getDisbursementDate() != null) {
                        swPojo.setDisbursementDate(formatDateTime.format(sw.getDisbursementDate()));
                    }
                    // get list produk pembiayaan SW
                    List<SWProductMapPOJO> swProductList = new ArrayList<>();
                    List<SWProductMapping> productList = swService.findProductMapBySwId(sw.getSwId());
                    for (SWProductMapping product : productList) {
                        SWProductMapPOJO swProduct = new SWProductMapPOJO();
                        swProduct.setSwProductId(
                                FieldValidationUtil.stringRequestValueValidation(product.getLocalId()));
                        if (product.getProductId() != null) {
                            if (product.getProductId().getProductId() != null) {
                                swProduct.setProductId(product.getProductId().getProductId().toString());
                                LoanProduct loanProduct = swService.findByProductId(swProduct.getProductId());
                                swProduct.setIir(FieldValidationUtil.bigDecimalValueResponseValidation(loanProduct.getIir()).toString());
                            }
                        }
                        if (product.getPlafon() != null) {
                            swProduct.setSelectedPlafon(product.getPlafon().toString());
                        }
                        if (product.getRecommendedProductId() != null) {
                            swProduct.setRecommendationProductId(product.getRecommendedProductId().toString());
                        }
                        if (product.getRecommendedPlafon() != null) {
                            swProduct.setRecommendationSelectedPlafon(product.getRecommendedPlafon().toString());
                        }
                        swProductList.add(swProduct);
                    }
                    swPojo.setSwProducts(swProductList);
                    List<AP3R> ap3rList = ap3RService.findAp3rListBySwId(sw.getSwId());
                    // get profil SW
                    SWProfilePOJO profile = new SWProfilePOJO();
                    if (sw.getCustomerId() != null) {
                        swPojo.setCustomerId(sw.getCustomerId().toString());
                        Customer customer = customerService.findById(sw.getCustomerId().toString());
                        if (customer != null) {
                            profile.setLongName(customer.getCustomerName());
                            swPojo.setCustomerCif(customer.getCustomerCifNumber());
                            swPojo.setSentraId(customer.getGroup().getSentra().getSentraId().toString());
                            swPojo.setSentraName(customer.getGroup().getSentra().getSentraName());
                            swPojo.setGroupId(customer.getGroup().getGroupId().toString());
                            swPojo.setGroupName(customer.getGroup().getGroupName());
                            swPojo.setCustomerExists("true");
                            for (AP3R ap3r : ap3rList) {
                                Loan loan = loanService.findByAp3rAndCustomer(ap3r.getAp3rId(), customer);
                                if (loan != null) {
                                    swPojo.setLoanExists("true");
                                } else {
                                    swPojo.setLoanExists("false");
                                }
                            }
                        }
                    } else {
                        swPojo.setCustomerExists("false");
                    }
                    swPojo.setIdCardNumber(sw.getCustomerIdNumber());
                    profile.setAlias(sw.getCustomerAliasName());
                    swPojo.setIdCardName(sw.getCustomerIdName());
                    profile.setGender(FieldValidationUtil.stringToZeroValidation(sw.getGender()));
                    profile.setReligion(FieldValidationUtil.stringToZeroValidation(sw.getReligion()));
                    profile.setBirthPlace(sw.getBirthPlace());
                    profile.setBirthPlaceRegencyName(sw.getBirthPlaceRegencyName());
                    if (sw.getBirthDate() != null)
                        profile.setBirthDay(formatter.format(sw.getBirthDate()));
                    if (sw.getIdExpiryDate() != null) {
                        profile.setIdCardExpired(formatter.format(sw.getIdExpiryDate()));
                    } else {
                        profile.setIdCardExpired("");
                    }
                    profile.setMarriedStatus(FieldValidationUtil.stringToZeroValidation(sw.getMaritalStatus()));
                    profile.setLongName(sw.getCustomerName());
                    profile.setWorkStatus(FieldValidationUtil.stringToZeroValidation(sw.getWorkType()));
                    if (sw.getCreatedDate() != null)
                        swPojo.setCreatedAt(formatter.format(sw.getCreatedDate()));
                    profile.setPhoneNumber(sw.getPhoneNumber());
                    profile.setNpwp(sw.getNpwp());
                    profile.setMotherName(sw.getMotherName());
                    profile.setDependants(FieldValidationUtil.integerValueResponseValidation(sw.getDependentCount()));
                    profile.setEducation(FieldValidationUtil.stringToZeroValidation(sw.getEducation()));
                    profile.setIsLifeTime(FieldValidationUtil.booleanResponseValueValidation(sw.getLifeTime()));
                    swPojo.setProfile(profile);
                    // get alamat SW
                    SWAddressPOJO address = new SWAddressPOJO();
                    if (sw.getSwLocation() != null)
                        address.setName(sw.getSwLocation());
                    else
                        address.setName(sw.getAddress());
                    address.setStreet(sw.getRtrw());
                    address.setPostCode(sw.getPostalCode());
                    address.setVillageId(FieldValidationUtil.stringToZeroValidation(sw.getKelurahan()));
                    address.setRegencyId(FieldValidationUtil.stringToZeroValidation(sw.getKecamatan()));
                    address.setProvinceId(FieldValidationUtil.stringToZeroValidation(sw.getProvince()));
                    address.setDistrictId(FieldValidationUtil.stringToZeroValidation(sw.getCity()));
                    address.setAdditionalAddress(sw.getDomisiliAddress());
                    address.setPlaceOwnerShip(sw.getHouseType());
                    address.setPlaceCertificate(
                            FieldValidationUtil.charValueResponseValidation(sw.getHouseCertificate()));
                    swPojo.setAddress(address);
                    // get pasangan SW
                    SWSpousePOJO partner = new SWSpousePOJO();
                    partner.setName(sw.getCoupleName());
                    partner.setBirthPlace(sw.getCoupleBirthPlaceRegencyName());
                    partner.setBirthPlaceId(sw.getCoupleBirthPlace());
                    if (sw.getCoupleBirthDate() != null)
                        partner.setBirthDay(formatter.format(sw.getCoupleBirthDate()));
                    if (sw.getCoupleJob() != null) {
                        if (sw.getCoupleJob().equals('K')) {
                            partner.setJob("Karyawan");
                        } else if (sw.getCoupleJob().equals('W')) {
                            partner.setJob("Wiraswasta");
                        } else if (sw.getCoupleJob().equals('M')) {
                            partner.setJob("Musiman");
                        } else {
                            partner.setJob("");
                        }
                    } else {
                        partner.setJob("");
                    }
                    swPojo.setPartner(partner);
                    // get usaha SW
                    SWBusinessPOJO business = new SWBusinessPOJO();
                    business.setType(sw.getBusinessField());
                    business.setDesc(sw.getBusinessDescription());
                    business.setBusinessOwnership(
                            FieldValidationUtil.charValueResponseValidation(sw.getBusinessOwnerStatus()));
                    business.setBusinessShariaCompliance(
                            FieldValidationUtil.charValueResponseValidation(sw.getBusinessShariaType()));
                    business.setBusinessLocation(
                            FieldValidationUtil.charValueResponseValidation(sw.getBusinessLocation()));
                    business.setName(sw.getBusinessName());
                    business.setAddress(sw.getBusinessAddress());
                    business.setAgeYear(FieldValidationUtil.integerValueResponseValidation(sw.getBusinessAgeYear()));
                    business.setAgeMonth(FieldValidationUtil.integerValueResponseValidation(sw.getBusinessAgeMonth()));
                    business.setBusinessRunner(
                            FieldValidationUtil.charValueResponseValidation(sw.getBusinessWorkStatus()));
                    business.setHasOtherBankLoan(FieldValidationUtil.charValueResponseValidation(sw.getHasOtherBankLoan()));
                    business.setOtherBankName(sw.getOtherBankLoanName());
                    swPojo.setBusiness(business);
                    // get parameter kalkulasi
                    SWCalculationVariablePOJO calcVariable = new SWCalculationVariablePOJO();
                    calcVariable.setJenisSiklus(FieldValidationUtil.charValueResponseValidation(sw.getBusinessCycle()));
                    calcVariable.setHariOperasional(sw.getBusinessDaysOperation());
                    calcVariable.setPendapatanRamai(
                            FieldValidationUtil.bigDecimalValueResponseValidation(sw.getOneDayBusyIncome()).toString());
                    calcVariable.setJumlahhariramai(
                            FieldValidationUtil.integerValueResponseValidation(sw.getTotalDaysInMonthBusy()));
                    calcVariable.setPendapatanSepi(
                            FieldValidationUtil.bigDecimalValueResponseValidation(sw.getOneDayLessIncome()).toString());
                    calcVariable.setJumlahharisepi(
                            FieldValidationUtil.integerValueResponseValidation(sw.getTotalDaysInMonthLess()));
                    calcVariable.setPendapatanPerbulanRamaiSepi(FieldValidationUtil
                            .bigDecimalValueResponseValidation(sw.getTotalLessBusyIncome()).toString());
                    if (sw.getFirstDayIncome() != null)
                        calcVariable.setPeriode1(sw.getFirstDayIncome().toString());
                    if (sw.getSecondDayIncome() != null)
                        calcVariable.setPeriode2(sw.getSecondDayIncome().toString());
                    if (sw.getThirdDayIncome() != null)
                        calcVariable.setPeriode3(sw.getThirdDayIncome().toString());
                    calcVariable.setPendapatanPerbulan3Periode(FieldValidationUtil
                            .bigDecimalValueResponseValidation(sw.getThreePeriodIncome()).toString());
                    calcVariable.setPendapatanRata2TigaPeriode(FieldValidationUtil
                            .bigDecimalValueResponseValidation(sw.getAvgThreePeriodIncome()).toString());
                    if (sw.getOpenTime() != null)
                        calcVariable.setJamBuka(formatTime.format(sw.getOpenTime()));
                    if (sw.getCloseTime() != null)
                        calcVariable.setJamTutup(formatTime.format(sw.getCloseTime()));
                    calcVariable.setWaktuKerja(FieldValidationUtil.integerValueResponseValidation(sw.getWorkTime()));
                    calcVariable.setWaktukerjaSetelahbuka(
                            FieldValidationUtil.integerValueResponseValidation(sw.getWorkTimeAfterOpen()));
                    if (sw.getCashOpenTime() != null)
                        calcVariable.setUangkasBoxdiBuka(sw.getCashOpenTime().toString());
                    if (sw.getCashCurrentTime() != null)
                        calcVariable.setUangKasBoxSekarang(sw.getCashCurrentTime().toString());
                    if (sw.getCashExpense() != null)
                        calcVariable.setUangBelanja(sw.getCashExpense().toString());
                    calcVariable.setPendapatanPerbulanPerkiraanSehari(FieldValidationUtil
                            .bigDecimalValueResponseValidation(sw.getOneDayPredictIncome()).toString());
                    calcVariable.setPendapatanPerbulanKasSehari(
                            FieldValidationUtil.bigDecimalValueResponseValidation(sw.getOneDayCashIncome()).toString());
                    List<OtherItemCalculation> itemPendapatanLainList = swService.findOtherItemCalcBySwId(sw.getSwId());
                    List<SWOtherItemCalculationPOJO> itemCalcLainList = new ArrayList<>();
                    for (OtherItemCalculation itemPendapatanLain : itemPendapatanLainList) {
                        SWOtherItemCalculationPOJO itemCalcLain = new SWOtherItemCalculationPOJO();
                        itemCalcLain.setName(itemPendapatanLain.getName());
                        itemCalcLain.setAmount(FieldValidationUtil
                                .bigDecimalValueResponseValidation(itemPendapatanLain.getAmount()).toString());
                        if (itemPendapatanLain.getPeriode() != null)
                            itemCalcLain.setPeriode(itemPendapatanLain.getPeriode().toString());
                        else
                            itemCalcLain.setPeriode("0");
                        itemCalcLain.setPendapatanPerbulan(FieldValidationUtil
                                .bigDecimalValueResponseValidation(itemPendapatanLain.getPendapatanPerbulan())
                                .toString());
                        itemCalcLainList.add(itemCalcLain);
                    }
                    calcVariable.setPendapatanLainnya(
                            FieldValidationUtil.bigDecimalValueResponseValidation(sw.getOtherIncome()).toString());
                    calcVariable.setItemCalcLains(itemCalcLainList);
                    swPojo.setCreatedBy(sw.getCreatedBy());
                    swPojo.setCalcVariable(calcVariable);
                    List<SWDirectBuyPOJO> directBuyList = new ArrayList<>();
                    List<DirectBuyThings> directBuyProductList = swService.findProductBySwId(sw);
                    for (DirectBuyThings directBuyProduct : directBuyProductList) {
                        SWDirectBuyPOJO product = new SWDirectBuyPOJO();
                        product.setFrequency(
                                FieldValidationUtil.integerValueResponseValidation(directBuyProduct.getFrequency()));
                        product.setIndex(
                                FieldValidationUtil.integerValueResponseValidation(directBuyProduct.getIndex()));
                        product.setItemName(directBuyProduct.getNamaBarang());
                        product.setPrice(
                                FieldValidationUtil.bigDecimalValueResponseValidation(directBuyProduct.getPrice()));
                        product.setSellingPrice(FieldValidationUtil
                                .bigDecimalValueResponseValidation(directBuyProduct.getSellingPrice()));
                        product.setTotalItem(directBuyProduct.getTotalItem().toString());
                        product.setType(FieldValidationUtil.integerValueResponseValidation(directBuyProduct.getType()));
                        directBuyList.add(product);
                    }
                    swPojo.setDirectPurchasing(directBuyList);
                    List<SWAWGMBuyPOJO> awgmBuyList = new ArrayList<>();
                    List<AWGMBuyThings> awgmBuyProductList = swService.findAwgmProductBySwId(sw);
                    for (AWGMBuyThings awgmBuyProduct : awgmBuyProductList) {
                        SWAWGMBuyPOJO product = new SWAWGMBuyPOJO();
                        product.setFrequency(
                                FieldValidationUtil.integerValueResponseValidation(awgmBuyProduct.getFrequency()));
                        product.setIndex(FieldValidationUtil.integerValueResponseValidation(awgmBuyProduct.getIndex()));
                        product.setItemName(awgmBuyProduct.getNamaBarang());
                        product.setPrice(
                                FieldValidationUtil.bigDecimalValueResponseValidation(awgmBuyProduct.getBuyPrice()));
                        product.setSellingPrice(
                                FieldValidationUtil.bigDecimalValueResponseValidation(awgmBuyProduct.getSellPrice()));
                        product.setTotalItem(awgmBuyProduct.getTotal().toString());
                        product.setType(FieldValidationUtil.integerValueResponseValidation(awgmBuyProduct.getType()));
                        awgmBuyList.add(product);
                    }
                    swPojo.setAwgmPurchasing(awgmBuyList);
                    SWExpensePOJO expense = new SWExpensePOJO();
                    if (sw.getTransportCost() != null)
                        expense.setTransportUsaha(sw.getTransportCost().toString());
                    if (sw.getUtilityCost() != null)
                        expense.setUtilitasUsaha(sw.getUtilityCost().toString());
                    if (sw.getStaffSalary() != null)
                        expense.setGajiUsaha(sw.getStaffSalary().toString());
                    if (sw.getRentCost() != null)
                        expense.setSewaUsaha(sw.getRentCost().toString());
                    if (sw.getPrivateTransportCost() != null)
                        expense.setTransportNonUsaha(sw.getPrivateTransportCost().toString());
                    if (sw.getPrivateUtilityCost() != null)
                        expense.setUtitlitasNonUsaha(sw.getPrivateUtilityCost().toString());
                    if (sw.getEducationCost() != null)
                        expense.setPendidikanNonUsaha(sw.getEducationCost().toString());
                    if (sw.getHealthCost() != null)
                        expense.setKesehatanNonUsaha(sw.getHealthCost().toString());
                    if (sw.getFoodCost() != null)
                        expense.setMakanNonUsaha(sw.getFoodCost().toString());
                    if (sw.getInstallmentCost() != null)
                        expense.setAngsuranNonUsaha(sw.getInstallmentCost().toString());
                    if (sw.getOtherCost() != null)
                        expense.setLainlainNonUsaha(sw.getOtherCost().toString());
                    swPojo.setExpense(expense);
                    List<SWNeighborRecommendationPOJO> referenceList = new ArrayList<>();
                    List<NeighborRecommendation> neighborList = swService.findNeighborBySwId(sw);
                    for (NeighborRecommendation neighbor : neighborList) {
                        SWNeighborRecommendationPOJO reference = new SWNeighborRecommendationPOJO();
                        reference.setName(neighbor.getNeighborName());
                        reference.setAddress(neighbor.getNeighborAddress());
                        reference.setGoodNeighbour("" + neighbor.getNeighborRelation());
                        if (reference.getGoodNeighbour().equals("t")) {
                            reference.setGoodNeighbour("true");
                        } else {
                            reference.setGoodNeighbour("false");
                        }
                        reference.setVisitedByLandshark("" + neighbor.getNeighborOutstanding());
                        if (reference.getVisitedByLandshark().equals("t")) {
                            reference.setVisitedByLandshark("true");
                        } else {
                            reference.setVisitedByLandshark("false");
                        }
                        reference.setHaveBusiness("" + neighbor.getNeighborBusiness());
                        if (reference.getHaveBusiness().equals("t")) {
                            reference.setHaveBusiness("true");
                        } else {
                            reference.setHaveBusiness("false");
                        }
                        reference.setRecommended("" + neighbor.getNeighborRecomend());
                        if (reference.getRecommended().equals("t")) {
                            reference.setRecommended("true");
                        } else {
                            reference.setRecommended("false");
                        }
                        reference.setLengthOfStay(FieldValidationUtil.charValueResponseValidation(neighbor.getLengthOfStay()));
                        referenceList.add(reference);
                    }
                    swPojo.setReferenceList(referenceList);
                    swPojo.setStatus(sw.getStatus());
                    if (sw.getPmId() != null)
                        swPojo.setPmId(sw.getPmId().getPmId().toString());
                    List<SWApprovalHistoryPOJO> swApprovalList = new ArrayList<>();
                    List<SWUserBWMPMapping> swUserBwmpList = swService.findBySw(sw.getSwId());
                    for (SWUserBWMPMapping swUserBwmp : swUserBwmpList) {
                        SWApprovalHistoryPOJO swApproval = new SWApprovalHistoryPOJO();
                        User userApproval = userService.loadUserByUserId(swUserBwmp.getUserId());
                        if (userApproval != null) {
                            swApproval.setLevel(swUserBwmp.getLevel());
                            swApproval.setLimit(FieldValidationUtil.stringToZeroValidation(userApproval.getLimit()));
                            swApproval.setRole(userApproval.getRoleName());
                            swApproval.setName(userApproval.getName());
                            swApproval.setDate(swUserBwmp.getDate());
                            swApproval.setTanggal(formatDateTime.format(swUserBwmp.getCreatedDate()));
                            swApproval.setStatus(swUserBwmp.getStatus());
                            swApproval.setSupervisorId(userApproval.getUserId().toString());
                            swApprovalList.add(swApproval);
                        }
                    }
                    swPojo.setApprovalHistories(swApprovalList);
                    SwIdPhoto swIdPhoto = swService.getSwIdPhoto(sw.getSwId().toString());
                    if (swIdPhoto != null)
                        swPojo.setHasIdPhoto("true");
                    else
                        swPojo.setHasIdPhoto("false");
                    SwSurveyPhoto swSurveyPhoto = swService.getSwSurveyPhoto(sw.getSwId().toString());
                    if (swSurveyPhoto != null)
                        swPojo.setHasBusinessPlacePhoto("true");
                    else
                        swPojo.setHasBusinessPlacePhoto("false");
                    if (sw.getHasApprovedMs() != null) {
                        if (sw.getHasApprovedMs().equals(true))
                            swPojo.setHasApprovedMs("true");
                        else
                            swPojo.setHasApprovedMs("false");
                    } else
                        swPojo.setHasApprovedMs("false");
                    swResponses.add(swPojo);
                }
                log.debug("Finishing create response Survey Wawancara data");
                log.debug("Try to Update Terminal");
                Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                terminal.setSwProgress(PercentageSynchronizer.processSyncPercent(request.getPage(),
                        String.valueOf(swPage.getTotalPages()), terminal.getSwProgress()));
                terminalService.updateTerminal(terminal);
                log.debug("Finishing Update Terminal");
                final List<SurveyWawancara> listDeletedSw = swService.findIsDeletedSwList();
                List<String> deletedSwList = new ArrayList<>();
                for (SurveyWawancara deletedSw : listDeletedSw) {
                    String id = deletedSw.getLocalId().toString();
                    deletedSwList.add(id);
                }
                responseCode.setDeletedSwList(deletedSwList);
                responseCode.setCurrentTotal(String.valueOf(swPage.getContent().size()));
                responseCode.setGrandTotal(String.valueOf(swPage.getTotalElements()));
                responseCode.setTotalPage(String.valueOf(swPage.getTotalPages()));
                responseCode.setSwList(swResponses);
            }
        } catch (Exception e) {
            log.error("syncSW error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("syncSW error: " + e.getMessage());
        } finally {
            try {
                log.debug("Try to create Terminal Activity and Audit Log");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_SW);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.debug("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.debug("Updating Terminal Activity");
                    }
                });
                log.info("syncSW RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("syncSW saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }
        return responseCode;
    }

}