package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.ppi.FamilyDataPPI;
import id.co.telkomsigma.btpns.mprospera.model.ppi.PPI;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.AddPPIRequest;
import id.co.telkomsigma.btpns.mprospera.request.FamilyListRequest;
import id.co.telkomsigma.btpns.mprospera.response.AddPPIResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.FieldValidationUtil;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webServiceAddPPIController")
public class WebServiceAddPPIController extends GenericController {

    JsonUtils jsonUtils = new JsonUtils();

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private PPIService ppiService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private SWService swService;

    @Autowired
    private LoanService loanService;

    @RequestMapping(value = WebGuiConstant.PPI_SUBMIT_DATA_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    AddPPIResponse doAdd(@RequestBody AddPPIRequest request,
                         @PathVariable("apkVersion") String apkVersion) throws Exception {

        final AddPPIResponse responseCode = new AddPPIResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        responseCode.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
        try {
            log.info("inputPPI INCOMING MESSAGE : " + jsonUtils.toJson(request));
            // username , imei, session key, apk version validation
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + request.getUsername() + " ,imei : " + request.getImei() + " ,sessionKey : " + request.getSessionKey());
            } else {
                log.debug("validation using " + request.getUsername() + " success");
                log.debug("START PARSING DATA");
                PPI ppi;
                if (request.getPpiId() != null && request.getSwId() != null) {
                    if (!"".equals(request.getPpiId()) && !"".equals(request.getSwId())) {
                        ppi = ppiService.findPPIbyLocalId(request.getPpiId());
                        if (ppi != null && WebGuiConstant.ACTION_INSERT.equals(request.getAction())) {
                            responseCode.setResponseCode(WebGuiConstant.DUPLICATE_PPI);
                            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                            responseCode.setResponseMessage(label);
                            return responseCode;
                        }
                        Long swId = swService.findSWIDByLocalId(request.getSwId());
                        if (swId == null) {
                            responseCode.setResponseCode(WebGuiConstant.RC_UNKNOWN_SW_ID);
                            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                            responseCode.setResponseMessage(label);
                            return responseCode;
                        }
                        ppi = ppiService.findPPIbySwId(swId);
                        if (ppi != null && WebGuiConstant.ACTION_INSERT.equals(request.getAction())) {
                            responseCode.setResponseCode(WebGuiConstant.DUPLICATE_PPI);
                            String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                            responseCode.setResponseMessage(label);
                            return responseCode;
                        }
                        if (ppi == null) {
                            ppi = new PPI();
                        }
                        ppi.setRrn(request.getRetrievalReferenceNumber());
                        if (WebGuiConstant.ACTION_UPDATE.equals(request.getAction())) {
                            if (loanService.countLoanBySWId(swId) > 0) {
                                responseCode.setResponseCode(WebGuiConstant.UPDATE_PPI_NOT_PERMITTED);
                                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                responseCode.setResponseMessage(label);
                                return responseCode;
                            }
                            ppi.setUpdatedBy(request.getUsername());
                            ppi.setUpdatedDate(new Date());
                            List<FamilyDataPPI> familyDataPPIList = ppiService.findFamilyDataByPPIId(ppi.getPpiId());
                            for (FamilyDataPPI familyDataPPI : familyDataPPIList) {
                                familyDataPPI.setDeleted(true);
                                ppiService.saveFamilyData(familyDataPPI);
                            }
                        }
                        if (WebGuiConstant.ACTION_DELETE.equals(request.getAction())) {
                            if (loanService.countLoanBySWId(swId) > 0) {
                                responseCode.setResponseCode(WebGuiConstant.UPDATE_PPI_NOT_PERMITTED);
                                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                                responseCode.setResponseMessage(label);
                                return responseCode;
                            }
                            ppi.setDeleted(true);
                            ppi.setDeletedBy(request.getUsername());
                            ppi.setDeletedDate(new Date());
                        }
                        ppi.setSwId(swId);
                        ppi.setCreatedBy(request.getUsername());
                        ppi.setCreatedDate(new Date());
                        ppi.setFamilyNumber(Integer.parseInt(
                                FieldValidationUtil.integerRequestValueValidation(request.getJumlahAnggotaKeluarga())));
                        if (request.getKeluargaBersekolah() != null) {
                            ppi.setFamilyInSchool(request.getKeluargaBersekolah().charAt(0));
                        }
                        ppi.setWifeEducation(request.getPendidikanKepalaKeluarga());
                        ppi.setHusbandJob(request.getPekerjaanKepalaKeluarga());
                        if (request.getJenisLantai() != null) {
                            ppi.setFloorType(request.getJenisLantai().charAt(0));
                        }
                        ppi.setToiletType(request.getJenisWC());
                        if (request.getBahanBakarUtama() != null) {
                            ppi.setMainGas(request.getBahanBakarUtama().charAt(0));
                        }
                        if (request.getTabungGas() != null) {
                            ppi.setHasGasTube(request.getTabungGas().charAt(0));
                        }
                        if (request.getKulkas() != null) {
                            ppi.setHasRefrigerator(request.getKulkas().charAt(0));
                        }
                        if (request.getSepedaMotor() != null) {
                            ppi.setHasMotorcycle(request.getSepedaMotor().charAt(0));
                        }
                        ppi.setStatus(WebGuiConstant.STATUS_DONE);
                        ppi.setPpiLocalId(request.getPpiId());
                        log.debug("TRY TO SAVE PPI DATA");
                        ppiService.savePPI(ppi);
                        responseCode.setPpiId(ppi.getPpiLocalId());
                        log.debug("SAVE PPI SUCCESS");
                        SurveyWawancara sw = swService.getSWById(swId.toString());
                        if (sw != null) {
                            if ("delete".equals(request.getAction())) {
                                sw.setHasPPI(false);
                            } else {
                                sw.setHasPPI(true);
                            }
                            swService.save(sw);
                        }
                        if (ppi.getPpiId() != null) {
                            for (FamilyListRequest familyListRequest : request.getFamilyList()) {
                                FamilyDataPPI familyDataPPI;
                                familyDataPPI = ppiService.findFamilyDataByLocalId(familyListRequest.getFamilyId());
                                if (familyDataPPI != null && WebGuiConstant.ACTION_INSERT.equals(familyListRequest.getAction())) {
                                    log.error("DUPLICATE FAMILY DATA");
                                } else {
                                    if (familyDataPPI == null) {
                                        familyDataPPI = new FamilyDataPPI();
                                    }
                                    if (WebGuiConstant.ACTION_UPDATE.equals(familyListRequest.getAction())) {
                                        familyDataPPI.setUpdatedBy(request.getUsername());
                                        familyDataPPI.setUpdatedDate(new Date());
                                    }
                                    if (WebGuiConstant.ACTION_DELETE.equals(familyListRequest.getAction())) {
                                        familyDataPPI.setDeleted(true);
                                        familyDataPPI.setDeletedBy(request.getUsername());
                                        familyDataPPI.setDeletedDate(new Date());
                                    }
                                    familyDataPPI.setCreatedBy(request.getUsername());
                                    familyDataPPI.setCreatedDate(new Date());
                                    familyDataPPI.setName(familyListRequest.getName());
                                    familyDataPPI.setPpiId(ppi.getPpiId());
                                    familyDataPPI.setFamilyStatus(familyListRequest.getFamilyStatus());
                                    if (familyListRequest.getDifferentBusiness() != null) {
                                        familyDataPPI.setHasDifferentBusiness(familyListRequest.getDifferentBusiness().charAt(0));
                                    }
                                    familyDataPPI.setAge(Integer.parseInt(
                                            FieldValidationUtil.integerRequestValueValidation(familyListRequest.getAge())));
                                    if (familyListRequest.getEducationStatus() != null) {
                                        familyDataPPI.setStillInSchool(familyListRequest.getEducationStatus().charAt(0));
                                    }
                                    familyDataPPI.setFamilyDataPpiLocalId(familyListRequest.getFamilyId());
                                    log.debug("TRY TO SAVE FAMILY DATA");
                                    ppiService.saveFamilyData(familyDataPPI);
                                    log.debug("SAVE FAMILY DATA SUCCESS");
                                }
                            }
                        }
                    } else {
                        responseCode.setResponseCode(WebGuiConstant.PPI_NULL);
                        String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                        responseCode.setResponseMessage(label);
                        return responseCode;
                    }
                } else {
                    responseCode.setResponseCode(WebGuiConstant.PPI_NULL);
                    String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                    responseCode.setResponseMessage(label);
                    return responseCode;
                }
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
            }
        } catch (Exception e) {
            log.error("inputPPI ERROR METHOD doAdd,Class WebServiceInputPPIController :" + e.getMessage(), e);
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("inputPPI error: " + e.getMessage());
        } finally {
            try {
                log.debug("Trying to CREATE Terminal Activity....");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_MODY_inputPPI);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                        log.debug("TERMINAL ACTIVITY LOGGING SUCCESS...");
                        log.debug("Updating Terminal Activity");
                    }
                });
                log.info("inputPPI RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("inputPPI TerminalActivity ERROR :" + e.getMessage());
            }
        }
        return responseCode;
    }

}