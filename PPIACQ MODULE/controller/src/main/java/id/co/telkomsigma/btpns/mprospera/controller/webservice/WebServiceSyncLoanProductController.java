package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;
import id.co.telkomsigma.btpns.mprospera.model.sw.ProductPlafond;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.ProductListRequest;
import id.co.telkomsigma.btpns.mprospera.response.LoanProductResponse;
import id.co.telkomsigma.btpns.mprospera.response.SyncProductResponse;
import id.co.telkomsigma.btpns.mprospera.service.SWService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalActivityService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.WSValidationService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webServiceSyncLoanProductController")
public class WebServiceSyncLoanProductController extends GenericController {

    JsonUtils jsonUtils = new JsonUtils();

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private SWService swService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_LOAN_PRODUCT_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SyncProductResponse doProductList(@RequestBody final ProductListRequest request,
                                      @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        String page = request.getPage().toString();
        String countData = request.getGetCountData().toString();
        final SyncProductResponse responseCode = new SyncProductResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        responseCode.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
        try {
            log.info("listProduct INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(username, imei, sessionKey, apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : " + sessionKey);
            } else {
                log.debug("Validation success, get Loan Product data");
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                final Page<LoanProduct> productPage = swService.getProduct(page, countData);
                log.debug("Finishing get Loan Product data");
                final List<LoanProductResponse> productResponses = new ArrayList<LoanProductResponse>();
                for (final LoanProduct loanProduct : productPage) {
                    LoanProductResponse product = new LoanProductResponse();
                    product.setProductId(loanProduct.getProductId());
                    product.setProductName(loanProduct.getProductName());
                    product.setJumlahAngsur(loanProduct.getInstallmentCount());
                    product.setTenorMinggu(loanProduct.getTenor());
                    product.setTipePembiayaan(loanProduct.getLoanType());
                    product.setFrekuensiAngsurSatuan(loanProduct.getInstallmentFreqTime());
                    product.setFrekuensiAngsur(loanProduct.getInstallmentFreqCount());
                    product.setStatus(loanProduct.getStatus());
                    if (loanProduct.getMargin() != null) {
                        product.setMarjin(loanProduct.getMargin().abs(new MathContext(8, RoundingMode.HALF_EVEN)));
                    } else {
                        product.setMarjin(loanProduct.getMargin().ZERO);
                    }
                    List<ProductPlafond> plafonList = swService.findByProductId(loanProduct);
                    List<BigDecimal> plafonNominalList = new ArrayList<>();
                    for (ProductPlafond plafon : plafonList) {
                        BigDecimal nominal;
                        nominal = plafon.getPlafond();
                        if (plafon.getPlafond() == null) {
                            nominal = BigDecimal.ZERO;
                        }
                        plafonNominalList.add(nominal);
                    }
                    product.setPlafon(plafonNominalList);
                    product.setProductRate(loanProduct.getProductRate());
                    product.setIir(loanProduct.getIir());
                    productResponses.add(product);
                }
                log.debug("Finishing create response Loan Product data");
                responseCode.setCurrentTotal(String.valueOf(productPage.getContent().size()));
                responseCode.setGrandTotal(String.valueOf(productPage.getTotalElements()));
                responseCode.setTotalPage(String.valueOf(productPage.getTotalPages()));
                responseCode.setProductList(productResponses);
            }
        } catch (Exception e) {
            log.error("listProduct error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("listProduct error: " + e.getMessage());
        } finally {
            try {
                log.debug("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_listProduct);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.debug("Finishing create Terminal Activity");
                log.info("listProduct RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("listProduct saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }
        return responseCode;
    }

}