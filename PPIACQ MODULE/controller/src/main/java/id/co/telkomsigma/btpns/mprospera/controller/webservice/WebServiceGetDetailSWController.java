package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.WismaPOJO;
import id.co.telkomsigma.btpns.mprospera.pojo.sw.*;
import id.co.telkomsigma.btpns.mprospera.request.DetailSWRequest;
import id.co.telkomsigma.btpns.mprospera.response.SWDetailResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.FieldValidationUtil;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webServiceGetDetailSWController")
public class WebServiceGetDetailSWController extends GenericController {

    JsonUtils jsonUtils = new JsonUtils();

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private SWService swService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private AP3RService ap3RService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private UserService userService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @RequestMapping(value = WebGuiConstant.TERMINAL_GET_DETAIL_SW_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SWDetailResponse getDetailSw(@RequestBody final DetailSWRequest request,
                                 @PathVariable("apkVersion") String apkVersion) throws Exception {

        SimpleDateFormat formatDateTime = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        final SWDetailResponse responseCode = new SWDetailResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        responseCode.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
        try {
            log.info("getDetailSW INCOMING MESSAGE : " + jsonUtils.toJson(request));
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                log.debug("Validation success, get Survey Wawancara data");
                responseCode.setResponseMessage("SUKSES");
                SurveyWawancara swDetail = swService.findSwByLocalId(request.getSwId());
                log.debug("Finishing get Survey Wawancara data");
                SWPOJO swPojo = new SWPOJO();
                swPojo.setSwId(FieldValidationUtil.stringRequestValueValidation(swDetail.getLocalId()));
                swPojo.setSwPPI(FieldValidationUtil.booleanResponseValueValidation(swDetail.getSwPPI()));
                swPojo.setDayLight(swDetail.getTime());
                swPojo.setSwLocation(swDetail.getSwLocation());
                if (swDetail.getNasabahPotensial() != null) {
                    if (swDetail.getNasabahPotensial().equals(true)) {
                        swPojo.setIsNasabahPotensial("true");
                    } else {
                        swPojo.setIsNasabahPotensial("false");
                    }
                } else {
                    swPojo.setIsNasabahPotensial("false");
                }
                // get wisma SW
                WismaPOJO wisma = new WismaPOJO();
                Location wismaSw = areaService.findLocationById(swDetail.getWismaId());
                if (wismaSw != null) {
                    wisma.setOfficeId(wismaSw.getLocationId());
                    wisma.setOfficeCode(wismaSw.getLocationCode());
                    wisma.setOfficeName(wismaSw.getName());
                    swPojo.setWisma(wisma);
                }
                log.debug("Finish get wisma");
                swPojo.setCustomerType(
                        FieldValidationUtil.charValueResponseValidation(swDetail.getCustomerRegistrationType()));
                if (swDetail.getSurveyDate() != null)
                    swPojo.setSurveyDate(formatDateTime.format(swDetail.getSurveyDate()));
                if (swDetail.getDisbursementDate() != null) {
                    swPojo.setDisbursementDate(formatDateTime.format(swDetail.getDisbursementDate()));
                }
                // get list produk pembiayaan SW
                List<SWProductMapPOJO> swProductList = new ArrayList<>();
                List<SWProductMapping> productList = swService.findProductMapBySwId(swDetail.getSwId());
                if (productList != null) {
                    for (SWProductMapping product : productList) {
                        SWProductMapPOJO swProduct = new SWProductMapPOJO();
                        swProduct
                                .setSwProductId(FieldValidationUtil.stringRequestValueValidation(product.getLocalId()));
                        swProduct.setProductId(product.getProductId().getProductId().toString());
                        if (product.getPlafon() != null) {
                            swProduct.setSelectedPlafon(product.getPlafon().toString());
                        }
                        if (product.getRecommendedProductId() != null) {
                            swProduct.setRecommendationProductId(product.getRecommendedProductId().toString());
                        }
                        if (product.getRecommendedPlafon() != null) {
                            swProduct.setRecommendationSelectedPlafon(product.getRecommendedPlafon().toString());
                        }
                        swProductList.add(swProduct);
                    }
                    swPojo.setSwProducts(swProductList);
                }
                log.debug("Finish get Product");
                List<AP3R> ap3rList = ap3RService.findAp3rListBySwId(swDetail.getSwId());
                // get profil SW
                SWProfilePOJO profile = new SWProfilePOJO();
                if (swDetail.getCustomerId() != null) {
                    swPojo.setCustomerId(swDetail.getCustomerId().toString());
                    Customer customer = customerService.findById(swDetail.getCustomerId().toString());
                    profile.setLongName(customer.getCustomerName());
                    swPojo.setCustomerCif(customer.getCustomerCifNumber());
                    swPojo.setSentraId(customer.getGroup().getSentra().getSentraId().toString());
                    swPojo.setSentraName(customer.getGroup().getSentra().getSentraName());
                    swPojo.setGroupId(customer.getGroup().getGroupId().toString());
                    swPojo.setGroupName(customer.getGroup().getGroupName());
                    swPojo.setCustomerExists("true");
                    for (AP3R ap3r : ap3rList) {
                        Loan loan = loanService.findByAp3rAndCustomer(ap3r.getAp3rId(), customer);
                        if (loan != null) {
                            swPojo.setLoanExists("true");
                        } else {
                            swPojo.setLoanExists("false");
                        }
                    }
                } else {
                    swPojo.setCustomerExists("false");
                }
                swPojo.setIdCardNumber(swDetail.getCustomerIdNumber());
                profile.setAlias(swDetail.getCustomerAliasName());
                swPojo.setIdCardName(swDetail.getCustomerIdName());
                profile.setGender(FieldValidationUtil.stringToZeroValidation(swDetail.getGender()));
                profile.setReligion(FieldValidationUtil.stringToZeroValidation(swDetail.getReligion()));
                profile.setBirthPlace(swDetail.getBirthPlace());
                profile.setBirthPlaceRegencyName(swDetail.getBirthPlaceRegencyName());
                if (swDetail.getBirthDate() != null)
                    profile.setBirthDay(formatter.format(swDetail.getBirthDate()));
                if (swDetail.getIdExpiryDate() != null) {
                    profile.setIdCardExpired(formatter.format(swDetail.getIdExpiryDate()));
                } else {
                    profile.setIdCardExpired("");
                }
                profile.setMarriedStatus(FieldValidationUtil.stringToZeroValidation(swDetail.getMaritalStatus()));
                profile.setLongName(swDetail.getCustomerName());
                profile.setWorkStatus(FieldValidationUtil.stringToZeroValidation(swDetail.getWorkType()));
                if (swDetail.getCreatedDate() != null)
                    swPojo.setCreatedAt(formatter.format(swDetail.getCreatedDate()));
                profile.setPhoneNumber(swDetail.getPhoneNumber());
                profile.setNpwp(swDetail.getNpwp());
                profile.setMotherName(swDetail.getMotherName());
                profile.setDependants(FieldValidationUtil.integerValueResponseValidation(swDetail.getDependentCount()));
                profile.setEducation(FieldValidationUtil.stringToZeroValidation(swDetail.getEducation()));
                profile.setIsLifeTime(FieldValidationUtil.booleanResponseValueValidation(swDetail.getLifeTime()));
                swPojo.setProfile(profile);
                // get alamat SW
                SWAddressPOJO address = new SWAddressPOJO();
                if (swDetail.getSwLocation() != null)
                    address.setName(swDetail.getSwLocation());
                else
                    address.setName(swDetail.getAddress());
                address.setStreet(swDetail.getRtrw());
                address.setPostCode(swDetail.getPostalCode());
                address.setVillageId(FieldValidationUtil.stringToZeroValidation(swDetail.getKelurahan()));
                address.setRegencyId(FieldValidationUtil.stringToZeroValidation(swDetail.getKecamatan()));
                address.setProvinceId(FieldValidationUtil.stringToZeroValidation(swDetail.getProvince()));
                address.setDistrictId(FieldValidationUtil.stringToZeroValidation(swDetail.getCity()));
                address.setAdditionalAddress(swDetail.getDomisiliAddress());
                address.setPlaceOwnerShip(swDetail.getHouseType());
                address.setPlaceCertificate(
                        FieldValidationUtil.charValueResponseValidation(swDetail.getHouseCertificate()));

                swPojo.setAddress(address);
                // get pasangan SW
                SWSpousePOJO partner = new SWSpousePOJO();
                partner.setName(swDetail.getCoupleName());
                partner.setBirthPlace(swDetail.getCoupleBirthPlaceRegencyName());
                partner.setBirthPlaceId(swDetail.getCoupleBirthPlace());
                if (swDetail.getCoupleBirthDate() != null)
                    partner.setBirthDay(formatter.format(swDetail.getCoupleBirthDate()));
                if (swDetail.getCoupleJob() != null) {
                    if (swDetail.getCoupleJob().equals('K')) {
                        partner.setJob("Karyawan");
                    } else if (swDetail.getCoupleJob().equals('W')) {
                        partner.setJob("Wiraswasta");
                    } else if (swDetail.getCoupleJob().equals('M')) {
                        partner.setJob("Musiman");
                    } else {
                        partner.setJob("");
                    }
                } else {
                    partner.setJob("");
                }
                swPojo.setPartner(partner);
                // get usaha SW
                SWBusinessPOJO business = new SWBusinessPOJO();
                business.setType(swDetail.getBusinessField());
                business.setDesc(swDetail.getBusinessDescription());
                business.setBusinessOwnership(
                        FieldValidationUtil.charValueResponseValidation(swDetail.getBusinessOwnerStatus()));
                business.setBusinessShariaCompliance(
                        FieldValidationUtil.charValueResponseValidation(swDetail.getBusinessShariaType()));
                business.setBusinessLocation(
                        FieldValidationUtil.charValueResponseValidation(swDetail.getBusinessLocation()));
                business.setName(swDetail.getBusinessName());
                business.setAddress(swDetail.getBusinessAddress());
                business.setAgeYear(FieldValidationUtil.integerValueResponseValidation(swDetail.getBusinessAgeYear()));
                business.setAgeMonth(
                        FieldValidationUtil.integerValueResponseValidation(swDetail.getBusinessAgeMonth()));
                business.setBusinessRunner(
                        FieldValidationUtil.charValueResponseValidation(swDetail.getBusinessWorkStatus()));
                business.setHasOtherBankLoan(FieldValidationUtil.charValueResponseValidation(swDetail.getHasOtherBankLoan()));
                business.setOtherBankName(swDetail.getOtherBankLoanName());
                swPojo.setBusiness(business);
                // get parameter kalkulasi
                SWCalculationVariablePOJO calcVariable = new SWCalculationVariablePOJO();
                calcVariable
                        .setJenisSiklus(FieldValidationUtil.charValueResponseValidation(swDetail.getBusinessCycle()));
                calcVariable.setHariOperasional(swDetail.getBusinessDaysOperation());
                calcVariable.setPendapatanRamai(FieldValidationUtil
                        .bigDecimalValueResponseValidation(swDetail.getOneDayBusyIncome()).toString());
                calcVariable.setJumlahhariramai(
                        FieldValidationUtil.integerValueResponseValidation(swDetail.getTotalDaysInMonthBusy()));
                calcVariable.setPendapatanSepi(FieldValidationUtil
                        .bigDecimalValueResponseValidation(swDetail.getOneDayLessIncome()).toString());
                calcVariable.setJumlahharisepi(
                        FieldValidationUtil.integerValueResponseValidation(swDetail.getTotalDaysInMonthLess()));
                calcVariable.setPendapatanPerbulanRamaiSepi(FieldValidationUtil
                        .bigDecimalValueResponseValidation(swDetail.getTotalLessBusyIncome()).toString());
                if (swDetail.getFirstDayIncome() != null)
                    calcVariable.setPeriode1(swDetail.getFirstDayIncome().toString());
                if (swDetail.getSecondDayIncome() != null)
                    calcVariable.setPeriode2(swDetail.getSecondDayIncome().toString());
                if (swDetail.getThirdDayIncome() != null)
                    calcVariable.setPeriode3(swDetail.getThirdDayIncome().toString());
                calcVariable.setPendapatanPerbulan3Periode(FieldValidationUtil
                        .bigDecimalValueResponseValidation(swDetail.getThreePeriodIncome()).toString());
                calcVariable.setPendapatanRata2TigaPeriode(FieldValidationUtil
                        .bigDecimalValueResponseValidation(swDetail.getAvgThreePeriodIncome()).toString());
                if (swDetail.getOpenTime() != null)
                    calcVariable.setJamBuka(formatTime.format(swDetail.getOpenTime()));
                if (swDetail.getCloseTime() != null)
                    calcVariable.setJamTutup(formatTime.format(swDetail.getCloseTime()));
                calcVariable.setWaktuKerja(FieldValidationUtil.integerValueResponseValidation(swDetail.getWorkTime()));
                calcVariable.setWaktukerjaSetelahbuka(
                        FieldValidationUtil.integerValueResponseValidation(swDetail.getWorkTimeAfterOpen()));
                if (swDetail.getCashOpenTime() != null)
                    calcVariable.setUangkasBoxdiBuka(swDetail.getCashOpenTime().toString());
                if (swDetail.getCashCurrentTime() != null)
                    calcVariable.setUangKasBoxSekarang(swDetail.getCashCurrentTime().toString());
                if (swDetail.getCashExpense() != null)
                    calcVariable.setUangBelanja(swDetail.getCashExpense().toString());
                calcVariable.setPendapatanPerbulanPerkiraanSehari(FieldValidationUtil
                        .bigDecimalValueResponseValidation(swDetail.getOneDayPredictIncome()).toString());
                calcVariable.setPendapatanPerbulanKasSehari(FieldValidationUtil
                        .bigDecimalValueResponseValidation(swDetail.getOneDayCashIncome()).toString());
                List<OtherItemCalculation> itemPendapatanLainList = swService
                        .findOtherItemCalcBySwId(swDetail.getSwId());
                List<SWOtherItemCalculationPOJO> itemCalcLainList = new ArrayList<>();
                for (OtherItemCalculation itemPendapatanLain : itemPendapatanLainList) {
                    SWOtherItemCalculationPOJO itemCalcLain = new SWOtherItemCalculationPOJO();
                    itemCalcLain.setName(itemPendapatanLain.getName());
                    itemCalcLain.setAmount(FieldValidationUtil
                            .bigDecimalValueResponseValidation(itemPendapatanLain.getAmount()).toString());
                    if (itemPendapatanLain.getPeriode() != null)
                        itemCalcLain.setPeriode(itemPendapatanLain.getPeriode().toString());
                    else
                        itemCalcLain.setPeriode("0");
                    itemCalcLain.setPendapatanPerbulan(FieldValidationUtil
                            .bigDecimalValueResponseValidation(itemPendapatanLain.getPendapatanPerbulan()).toString());
                    itemCalcLainList.add(itemCalcLain);
                }
                calcVariable.setPendapatanLainnya(
                        FieldValidationUtil.bigDecimalValueResponseValidation(swDetail.getOtherIncome()).toString());
                calcVariable.setItemCalcLains(itemCalcLainList);
                swPojo.setCreatedBy(swDetail.getCreatedBy());
                swPojo.setCalcVariable(calcVariable);
                List<SWDirectBuyPOJO> directBuyList = new ArrayList<>();
                List<DirectBuyThings> directBuyProductList = swService.findProductBySwId(swDetail);
                for (DirectBuyThings directBuyProduct : directBuyProductList) {
                    SWDirectBuyPOJO product = new SWDirectBuyPOJO();
                    product.setFrequency(
                            FieldValidationUtil.integerValueResponseValidation(directBuyProduct.getFrequency()));
                    product.setIndex(FieldValidationUtil.integerValueResponseValidation(directBuyProduct.getIndex()));
                    product.setItemName(directBuyProduct.getNamaBarang());
                    product.setPrice(
                            FieldValidationUtil.bigDecimalValueResponseValidation(directBuyProduct.getPrice()));
                    product.setSellingPrice(
                            FieldValidationUtil.bigDecimalValueResponseValidation(directBuyProduct.getSellingPrice()));
                    product.setTotalItem(directBuyProduct.getTotalItem().toString());
                    product.setType(FieldValidationUtil.integerValueResponseValidation(directBuyProduct.getType()));
                    directBuyList.add(product);
                }
                swPojo.setDirectPurchasing(directBuyList);
                List<SWAWGMBuyPOJO> awgmBuyList = new ArrayList<>();
                List<AWGMBuyThings> awgmBuyProductList = swService.findAwgmProductBySwId(swDetail);
                for (AWGMBuyThings awgmBuyProduct : awgmBuyProductList) {
                    SWAWGMBuyPOJO product = new SWAWGMBuyPOJO();
                    product.setFrequency(
                            FieldValidationUtil.integerValueResponseValidation(awgmBuyProduct.getFrequency()));
                    product.setIndex(FieldValidationUtil.integerValueResponseValidation(awgmBuyProduct.getIndex()));
                    product.setItemName(awgmBuyProduct.getNamaBarang());
                    product.setPrice(
                            FieldValidationUtil.bigDecimalValueResponseValidation(awgmBuyProduct.getBuyPrice()));
                    product.setSellingPrice(
                            FieldValidationUtil.bigDecimalValueResponseValidation(awgmBuyProduct.getSellPrice()));
                    product.setTotalItem(awgmBuyProduct.getTotal().toString());
                    product.setType(FieldValidationUtil.integerValueResponseValidation(awgmBuyProduct.getType()));
                    awgmBuyList.add(product);
                }
                swPojo.setAwgmPurchasing(awgmBuyList);
                SWExpensePOJO expense = new SWExpensePOJO();
                if (swDetail.getTransportCost() != null)
                    expense.setTransportUsaha(swDetail.getTransportCost().toString());
                if (swDetail.getUtilityCost() != null)
                    expense.setUtilitasUsaha(swDetail.getUtilityCost().toString());
                if (swDetail.getStaffSalary() != null)
                    expense.setGajiUsaha(swDetail.getStaffSalary().toString());
                if (swDetail.getRentCost() != null)
                    expense.setSewaUsaha(swDetail.getRentCost().toString());
                if (swDetail.getPrivateTransportCost() != null)
                    expense.setTransportNonUsaha(swDetail.getPrivateTransportCost().toString());
                if (swDetail.getPrivateUtilityCost() != null)
                    expense.setUtitlitasNonUsaha(swDetail.getPrivateUtilityCost().toString());
                if (swDetail.getEducationCost() != null)
                    expense.setPendidikanNonUsaha(swDetail.getEducationCost().toString());
                if (swDetail.getHealthCost() != null)
                    expense.setKesehatanNonUsaha(swDetail.getHealthCost().toString());
                if (swDetail.getFoodCost() != null)
                    expense.setMakanNonUsaha(swDetail.getFoodCost().toString());
                if (swDetail.getInstallmentCost() != null)
                    expense.setAngsuranNonUsaha(swDetail.getInstallmentCost().toString());
                if (swDetail.getOtherCost() != null)
                    expense.setLainlainNonUsaha(swDetail.getOtherCost().toString());
                swPojo.setExpense(expense);
                List<SWNeighborRecommendationPOJO> referenceList = new ArrayList<>();
                List<NeighborRecommendation> neighborList = swService.findNeighborBySwId(swDetail);
                for (NeighborRecommendation neighbor : neighborList) {
                    SWNeighborRecommendationPOJO reference = new SWNeighborRecommendationPOJO();
                    reference.setName(neighbor.getNeighborName());
                    reference.setAddress(neighbor.getNeighborAddress());
                    reference.setGoodNeighbour("" + neighbor.getNeighborRelation());
                    if (reference.getGoodNeighbour().equals("t")) {
                        reference.setGoodNeighbour("true");
                    } else {
                        reference.setGoodNeighbour("false");
                    }
                    reference.setVisitedByLandshark("" + neighbor.getNeighborOutstanding());
                    if (reference.getVisitedByLandshark().equals("t")) {
                        reference.setVisitedByLandshark("true");
                    } else {
                        reference.setVisitedByLandshark("false");
                    }
                    reference.setHaveBusiness("" + neighbor.getNeighborBusiness());
                    if (reference.getHaveBusiness().equals("t")) {
                        reference.setHaveBusiness("true");
                    } else {
                        reference.setHaveBusiness("false");
                    }
                    reference.setRecommended("" + neighbor.getNeighborRecomend());
                    if (reference.getRecommended().equals("t")) {
                        reference.setRecommended("true");
                    } else {
                        reference.setRecommended("false");
                    }
                    reference.setLengthOfStay(FieldValidationUtil.charValueResponseValidation(neighbor.getLengthOfStay()));
                    referenceList.add(reference);
                }
                swPojo.setReferenceList(referenceList);
                swPojo.setStatus(swDetail.getStatus());
                if (swDetail.getPmId() != null)
                    swPojo.setPmId(swDetail.getPmId().getPmId().toString());
                List<SWApprovalHistoryPOJO> swApprovalList = new ArrayList<>();
                List<SWUserBWMPMapping> swUserBwmpList = swService.findBySw(swDetail.getSwId());
                for (SWUserBWMPMapping swUserBwmp : swUserBwmpList) {
                    SWApprovalHistoryPOJO swApproval = new SWApprovalHistoryPOJO();
                    User userApproval = userService.loadUserByUserId(swUserBwmp.getUserId());
                    if (userApproval != null) {
                        swApproval.setLevel(swUserBwmp.getLevel());
                        swApproval.setLimit(FieldValidationUtil.stringToZeroValidation(userApproval.getLimit()));
                        swApproval.setRole(userApproval.getRoleName());
                        swApproval.setName(userApproval.getName());
                        swApproval.setDate(swUserBwmp.getDate());
                        swApproval.setTanggal(formatDateTime.format(swUserBwmp.getCreatedDate()));
                        swApproval.setStatus(swUserBwmp.getStatus());
                        swApproval.setSupervisorId(userApproval.getUserId().toString());
                        swApprovalList.add(swApproval);
                    }
                }
                swPojo.setApprovalHistories(swApprovalList);
                SwIdPhoto swIdPhoto = swService.getSwIdPhoto(swDetail.getSwId().toString());
                if (swIdPhoto != null)
                    swPojo.setHasIdPhoto("true");
                else
                    swPojo.setHasIdPhoto("false");
                SwSurveyPhoto swSurveyPhoto = swService.getSwSurveyPhoto(swDetail.getSwId().toString());
                if (swSurveyPhoto != null)
                    swPojo.setHasBusinessPlacePhoto("true");
                else
                    swPojo.setHasBusinessPlacePhoto("false");
                if (swDetail.getHasApprovedMs() != null) {
                    if (swDetail.getHasApprovedMs().equals(true))
                        swPojo.setHasApprovedMs("true");
                    else
                        swPojo.setHasApprovedMs("false");
                } else
                    swPojo.setHasApprovedMs("false");
                responseCode.setSwDetail(swPojo);
                log.debug("Finishing create response Survey Wawancara data");
                log.debug("Try to Update Terminal");
            }
        } catch (Exception e) {
            log.error("getDetailSW error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("getDetailSW error: " + e.getMessage());
        } finally {
            try {
                log.debug("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_getDetailSW);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.info("getDetailSW RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("getDetailSW saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
            log.debug("Send Response to Device");
        }
        return responseCode;
    }

}