package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.ppi.PPI;
import id.co.telkomsigma.btpns.mprospera.model.sw.SWProductMapping;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.request.SyncPPIRequest;
import id.co.telkomsigma.btpns.mprospera.response.PPIResponse;
import id.co.telkomsigma.btpns.mprospera.response.SWProductMapResponse;
import id.co.telkomsigma.btpns.mprospera.response.SyncPPIResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("webServiceSyncPPIController")
public class WebServiceSyncPPIController extends GenericController {

    JsonUtils jsonUtils = new JsonUtils();

    @Autowired
    private WSValidationService wsValidationService;

    @Autowired
    private PPIService ppiService;

    @Autowired
    private SWService swService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private SentraService sentraService;

    @Autowired
    private AP3RService ap3RService;

    @Autowired
    private LoanService loanService;

    @Autowired
    private TerminalService terminalService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private TerminalActivityService terminalActivityService;

    @RequestMapping(value = WebGuiConstant.PPI_SYNC_DATA_REQUEST, method = {RequestMethod.POST})
    public @ResponseBody
    SyncPPIResponse doSyncPPI(@RequestBody final SyncPPIRequest request,
                              @PathVariable("apkVersion") String apkVersion) throws Exception {

        String username = request.getUsername();
        String imei = request.getImei();
        String sessionKey = request.getSessionKey();
        String countData = request.getGetCountData();
        String page = request.getPage();
        String startLookupDate = request.getStartLookupDate();
        String endLookupDate = request.getEndLookupDate();
        final SyncPPIResponse responseCode = new SyncPPIResponse();
        responseCode.setResponseCode(WebGuiConstant.RC_SUCCESS);
        responseCode.setRetrievalReferenceNumber(request.getRetrievalReferenceNumber());
        try {
            log.info("syncPPI INCOMING MESSAGE : " + jsonUtils.toJson(request));
            log.debug("VALIDATING REQUEST...");
            String validation = wsValidationService.wsValidation(request.getUsername(), request.getImei(),
                    request.getSessionKey(), apkVersion);
            if (!validation.equals(WebGuiConstant.RC_SUCCESS)) {
                responseCode.setResponseCode(validation);
                String label = getMessage("webservice.rc.label." + responseCode.getResponseCode());
                responseCode.setResponseMessage(label);
                log.error("Validation Failed for username : " + username + " ,imei : " + imei + " ,sessionKey : "
                        + sessionKey);
            } else {
                log.debug("Validation success, get PPI data");
                responseCode.setResponseMessage("SUKSES");
                final Page<PPI> ppiPage = ppiService.getPPI(request.getUsername(), page, countData,
                        startLookupDate, endLookupDate);
                log.debug("Finishing get PPI data");
                final List<PPIResponse> ppiResponses = new ArrayList<>();
                try {
                    if (ppiPage != null) {
                        log.debug("START PARSING DATA");
                        for (final PPI ppi : ppiPage) {
                            PPIResponse ppiPojo = new PPIResponse();
                            ppiPojo.setPpiId(ppi.getPpiLocalId());
                            ppiPojo.setStatus(ppi.getStatus());
                            ppiPojo.setCreatedBy(ppi.getCreatedBy());
                            SurveyWawancara sw = swService.getSWById(ppi.getSwId().toString());
                            if (sw != null) {
                                if (loanService.countLoanBySWId(sw.getSwId()) > 0) {
                                    ppiPojo.setHasLoan("true");
                                } else {
                                    ppiPojo.setHasLoan("false");
                                }
                                ppiPojo.setSwId(sw.getLocalId());
                                ppiPojo.setCustomerName(sw.getCustomerIdName());
                                ppiPojo.setCustomerAddress(sw.getAddress());
                                if (sw.getCustomerId() != null) {
                                    Customer customer = customerService.findById(sw.getCustomerId().toString());
                                    if (customer != null) {
                                        ppiPojo.setCifNumber(customer.getCustomerCifNumber());
                                        String sentraName = sentraService.findSentraNameById(customer.getGroup().getSentra().getSentraId());
                                        ppiPojo.setSentraName(sentraName);
                                    }
                                }
                                List<SWProductMapResponse> swProductMapResponseList = new ArrayList<>();
                                List<SWProductMapping> swProductMappingList = swService.findProductMapBySwId(sw.getSwId());
                                for (SWProductMapping swProductMapping : swProductMappingList) {
                                    SWProductMapResponse swProductMapResponse = new SWProductMapResponse();
                                    swProductMapResponse.setProductName(swProductMapping.getProductId().getProductName());
                                    swProductMapResponse.setPlafon(swProductMapping.getRecommendedPlafon());
                                    swProductMapResponseList.add(swProductMapResponse);
                                }
                                ppiPojo.setSwProducts(swProductMapResponseList);
                                ppiResponses.add(ppiPojo);
                            }

                        }
                        log.debug("Finishing create response PPI data");
                        final List<String> listDeletedPpi = ppiService.findDeletedPPI();
                        log.debug("Number of deleted PPI : " + listDeletedPpi.size());
                        responseCode.setDeletedPPIList(listDeletedPpi);
                        responseCode.setCurrentTotal(String.valueOf(ppiPage.getContent().size()));
                        responseCode.setGrandTotal(String.valueOf(ppiPage.getTotalElements()));
                        responseCode.setTotalPage(String.valueOf(ppiPage.getTotalPages()));
                        responseCode.setPpiList(ppiResponses);
                    }
                } catch (Exception e) {
                    log.error("syncPPI error: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        } catch (ParseException e) {
            log.error("syncPPI error: " + e.getMessage());
            e.printStackTrace();
            responseCode.setResponseCode(WebGuiConstant.RC_GENERAL_ERROR);
            responseCode.setResponseMessage("syncPPI error: " + e.getMessage());
        } finally {
            try {
                log.debug("Try to create Terminal Activity");
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei(request.getImei());
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_SYNC_SW);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(request.getRetrievalReferenceNumber().trim());
                        terminalActivity.setSessionKey(request.getSessionKey());
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername(request.getUsername().trim());
                        List<MessageLogs> messageLogs = new ArrayList<>();
                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, messageLogs);
                    }
                });
                log.debug("Finishing create Terminal Activity");
                log.info("syncPPI RESPONSE MESSAGE : " + jsonUtils.toJson(responseCode));
            } catch (Exception e) {
                log.error("syncPPI saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
        }
        return responseCode;
    }

}