package id.co.telkomsigma.btpns.mprospera.response;

public class AddPPIResponse extends BaseResponse {

    private String ppiId;

    public String getPpiId() {
        return ppiId;
    }

    public void setPpiId(String ppiId) {
        this.ppiId = ppiId;
    }
}
