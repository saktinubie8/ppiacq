package id.co.telkomsigma.btpns.mprospera.pojo.sw;

public class SWNeighborRecommendationPOJO {

    private String address;
    private String goodNeighbour;
    private String haveBusiness;
    private String name;
    private String recommended;
    private String visitedByLandshark;
    private String lengthOfStay;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGoodNeighbour() {
        return goodNeighbour;
    }

    public void setGoodNeighbour(String goodNeighbour) {
        this.goodNeighbour = goodNeighbour;
    }

    public String getHaveBusiness() {
        return haveBusiness;
    }

    public void setHaveBusiness(String haveBusiness) {
        this.haveBusiness = haveBusiness;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecommended() {
        return recommended;
    }

    public void setRecommended(String recommended) {
        this.recommended = recommended;
    }

    public String getVisitedByLandshark() {
        return visitedByLandshark;
    }

    public void setVisitedByLandshark(String visitedByLandshark) {
        this.visitedByLandshark = visitedByLandshark;
    }

    public String getLengthOfStay() {
        return lengthOfStay;
    }

    public void setLengthOfStay(String lengthOfStay) {
        this.lengthOfStay = lengthOfStay;
    }

}
