package id.co.telkomsigma.btpns.mprospera.response;

public class FamilyDataPPIResponse {

    private String familyId;
    private String name;
    private String familyStatus;
    private String differentBusiness;
    private String age;
    private String educationStatus;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyStatus() {
        return familyStatus;
    }

    public void setFamilyStatus(String familyStatus) {
        this.familyStatus = familyStatus;
    }

    public String getDifferentBusiness() {
        return differentBusiness;
    }

    public void setDifferentBusiness(String differentBusiness) {
        this.differentBusiness = differentBusiness;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getEducationStatus() {
        return educationStatus;
    }

    public void setEducationStatus(String educationStatus) {
        this.educationStatus = educationStatus;
    }

    public String getFamilyId() {
        return familyId;
    }

    public void setFamilyId(String familyId) {
        this.familyId = familyId;
    }
}
