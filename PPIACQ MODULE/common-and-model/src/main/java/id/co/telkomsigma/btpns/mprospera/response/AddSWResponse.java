package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class AddSWResponse extends BaseResponse {

    private String swId;
    private String localId;
    private List<ProductListResponse> swProducts;

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public List<ProductListResponse> getSwProducts() {
        return swProducts;
    }

    public void setSwProducts(List<ProductListResponse> swProducts) {
        this.swProducts = swProducts;
    }

}
