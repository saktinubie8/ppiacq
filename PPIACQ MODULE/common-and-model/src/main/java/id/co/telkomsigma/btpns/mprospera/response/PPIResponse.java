package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class PPIResponse {

    private String ppiId;
    private String swId;
    private String status;
    private String customerName;
    private String customerAddress;
    private String cifNumber;
    private String hasLoan;
    private List<SWProductMapResponse> swProducts;
    private String sentraName;
    private String createdBy;

    public String getPpiId() {
        return ppiId;
    }

    public void setPpiId(String ppiId) {
        this.ppiId = ppiId;
    }

    public String getSwId() {
        return swId;
    }

    public void setSwId(String swId) {
        this.swId = swId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCifNumber() {
        return cifNumber;
    }

    public void setCifNumber(String cifNumber) {
        this.cifNumber = cifNumber;
    }

    public List<SWProductMapResponse> getSwProducts() {
        return swProducts;
    }

    public void setSwProducts(List<SWProductMapResponse> swProducts) {
        this.swProducts = swProducts;
    }

    public String getSentraName() {
        return sentraName;
    }

    public void setSentraName(String sentraName) {
        this.sentraName = sentraName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getHasLoan() {
        return hasLoan;
    }

    public void setHasLoan(String hasLoan) {
        this.hasLoan = hasLoan;
    }
}