package id.co.telkomsigma.btpns.mprospera.model.ppi;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "T_PPI")
public class PPI extends GenericModel {

    private Long ppiId;
    private String ppiLocalId;
    private String rrn;
    private Long swId;
    private String createdBy;
    private Date createdDate;
    private String updatedBy;
    private Date updatedDate;
    private String deletedBy;
    private Date deletedDate;
    private String status;
    private Boolean isDeleted = false;
    private Integer familyNumber;
    private Character familyInSchool;
    private String wifeEducation;
    private String husbandJob;
    private Character floorType;
    private String toiletType;
    private Character mainGas;
    private Character hasGasTube;
    private Character hasRefrigerator;
    private Character hasMotorcycle;

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false, unique = true)
    public Long getPpiId() {
        return ppiId;
    }

    public void setPpiId(Long ppiId) {
        this.ppiId = ppiId;
    }

    @Column(name = "local_id")
    public String getPpiLocalId() {
        return ppiLocalId;
    }

    public void setPpiLocalId(String ppiLocalId) {
        this.ppiLocalId = ppiLocalId;
    }

    @Column(name = "rrn")
    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    @Column(name = "sw_id")
    public Long getSwId() {
        return swId;
    }

    public void setSwId(Long swId) {
        this.swId = swId;
    }

    @Column(name = "created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "created_date")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name = "updated_date")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Column(name = "is_deleted")
    public Boolean getDeleted() {
        return isDeleted;
    }

    public void setDeleted(Boolean deleted) {
        isDeleted = deleted;
    }

    @Column(name = "family_number")
    public Integer getFamilyNumber() {
        return familyNumber;
    }

    public void setFamilyNumber(Integer familyNumber) {
        this.familyNumber = familyNumber;
    }

    @Column(name = "family_in_school")
    public Character getFamilyInSchool() {
        return familyInSchool;
    }

    public void setFamilyInSchool(Character familyInSchool) {
        this.familyInSchool = familyInSchool;
    }

    @Column(name = "wife_education")
    public String getWifeEducation() {
        return wifeEducation;
    }

    public void setWifeEducation(String wifeEducation) {
        this.wifeEducation = wifeEducation;
    }

    @Column(name = "husband_job")
    public String getHusbandJob() {
        return husbandJob;
    }

    public void setHusbandJob(String husbandJob) {
        this.husbandJob = husbandJob;
    }

    @Column(name = "floor_type")
    public Character getFloorType() {
        return floorType;
    }

    public void setFloorType(Character floorType) {
        this.floorType = floorType;
    }

    @Column(name = "toilet_type")
    public String getToiletType() {
        return toiletType;
    }

    public void setToiletType(String toiletType) {
        this.toiletType = toiletType;
    }

    @Column(name = "main_gas")
    public Character getMainGas() {
        return mainGas;
    }

    public void setMainGas(Character mainGas) {
        this.mainGas = mainGas;
    }

    @Column(name = "has_gas_tube")
    public Character getHasGasTube() {
        return hasGasTube;
    }

    public void setHasGasTube(Character hasGasTube) {
        this.hasGasTube = hasGasTube;
    }

    @Column(name = "has_refrigerator")
    public Character getHasRefrigerator() {
        return hasRefrigerator;
    }

    public void setHasRefrigerator(Character hasRefrigerator) {
        this.hasRefrigerator = hasRefrigerator;
    }

    @Column(name = "has_motorcycle")
    public Character getHasMotorcycle() {
        return hasMotorcycle;
    }

    public void setHasMotorcycle(Character hasMotorcycle) {
        this.hasMotorcycle = hasMotorcycle;
    }

    @Column(name = "deleted_by")
    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    @Column(name = "deleted_date")
    public Date getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(Date deletedDate) {
        this.deletedDate = deletedDate;
    }

}
