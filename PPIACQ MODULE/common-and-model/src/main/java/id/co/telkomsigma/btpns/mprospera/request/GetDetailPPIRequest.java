package id.co.telkomsigma.btpns.mprospera.request;

public class GetDetailPPIRequest extends BaseRequest {

    private String username;
    private String imei;
    private String sessionKey;
    private String ppiId;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getPpiId() {
        return ppiId;
    }

    public void setPpiId(String ppiId) {
        this.ppiId = ppiId;
    }

}
