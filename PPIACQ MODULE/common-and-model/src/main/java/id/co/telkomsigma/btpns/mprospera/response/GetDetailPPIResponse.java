package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class GetDetailPPIResponse extends BaseResponse {

    private String jumlahAnggotaKeluarga;
    private String keluargaBersekolah;
    private String pendidikanKepalaKeluarga;
    private String pekerjaanKepalaKeluarga;
    private String jenisLantai;
    private String jenisWC;
    private String bahanBakarUtama;
    private String tabungGas;
    private String kulkas;
    private String sepedaMotor;
    private List<FamilyDataPPIResponse> familyList;

    public String getJumlahAnggotaKeluarga() {
        return jumlahAnggotaKeluarga;
    }

    public void setJumlahAnggotaKeluarga(String jumlahAnggotaKeluarga) {
        this.jumlahAnggotaKeluarga = jumlahAnggotaKeluarga;
    }

    public String getKeluargaBersekolah() {
        return keluargaBersekolah;
    }

    public void setKeluargaBersekolah(String keluargaBersekolah) {
        this.keluargaBersekolah = keluargaBersekolah;
    }

    public String getPendidikanKepalaKeluarga() {
        return pendidikanKepalaKeluarga;
    }

    public void setPendidikanKepalaKeluarga(String pendidikanKepalaKeluarga) {
        this.pendidikanKepalaKeluarga = pendidikanKepalaKeluarga;
    }

    public String getPekerjaanKepalaKeluarga() {
        return pekerjaanKepalaKeluarga;
    }

    public void setPekerjaanKepalaKeluarga(String pekerjaanKepalaKeluarga) {
        this.pekerjaanKepalaKeluarga = pekerjaanKepalaKeluarga;
    }

    public String getJenisLantai() {
        return jenisLantai;
    }

    public void setJenisLantai(String jenisLantai) {
        this.jenisLantai = jenisLantai;
    }

    public String getJenisWC() {
        return jenisWC;
    }

    public void setJenisWC(String jenisWC) {
        this.jenisWC = jenisWC;
    }

    public String getBahanBakarUtama() {
        return bahanBakarUtama;
    }

    public void setBahanBakarUtama(String bahanBakarUtama) {
        this.bahanBakarUtama = bahanBakarUtama;
    }

    public String getTabungGas() {
        return tabungGas;
    }

    public void setTabungGas(String tabungGas) {
        this.tabungGas = tabungGas;
    }

    public String getKulkas() {
        return kulkas;
    }

    public void setKulkas(String kulkas) {
        this.kulkas = kulkas;
    }

    public String getSepedaMotor() {
        return sepedaMotor;
    }

    public void setSepedaMotor(String sepedaMotor) {
        this.sepedaMotor = sepedaMotor;
    }

    public List<FamilyDataPPIResponse> getFamilyList() {
        return familyList;
    }

    public void setFamilyList(List<FamilyDataPPIResponse> familyList) {
        this.familyList = familyList;
    }

}
