package id.co.telkomsigma.btpns.mprospera.response;

import id.co.telkomsigma.btpns.mprospera.pojo.sw.SWPOJO;

import java.util.List;

public class SyncSWResponse extends BaseResponse {

    private String grandTotal;
    private String currentTotal;
    private String totalPage;
    private List<SWPOJO> swList;
    private List<String> deletedSwList;

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getCurrentTotal() {
        return currentTotal;
    }

    public void setCurrentTotal(String currentTotal) {
        this.currentTotal = currentTotal;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public List<SWPOJO> getSwList() {
        return swList;
    }

    public void setSwList(List<SWPOJO> swList) {
        this.swList = swList;
    }

    public List<String> getDeletedSwList() {
        return deletedSwList;
    }

    public void setDeletedSwList(List<String> deletedSwList) {
        this.deletedSwList = deletedSwList;
    }

}
