package id.co.telkomsigma.btpns.mprospera.response;

public class ProductListResponse {

    private String productId;
    private String recommendationProductId;
    private String swProductId;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getRecommendationProductId() {
        return recommendationProductId;
    }

    public void setRecommendationProductId(String recommendationProductId) {
        this.recommendationProductId = recommendationProductId;
    }

    public String getSwProductId() {
        return swProductId;
    }

    public void setSwProductId(String swProductId) {
        this.swProductId = swProductId;
    }
}
