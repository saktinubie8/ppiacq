package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;

public class SWProductMapResponse {

    private String productName;
    private BigDecimal plafon;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getPlafon() {
        return plafon;
    }

    public void setPlafon(BigDecimal plafon) {
        this.plafon = plafon;
    }

}
