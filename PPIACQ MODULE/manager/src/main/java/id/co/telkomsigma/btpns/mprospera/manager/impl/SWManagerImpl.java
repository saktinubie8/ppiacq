package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.dao.*;
import id.co.telkomsigma.btpns.mprospera.model.sw.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.manager.SWManager;
import org.springframework.transaction.annotation.Transactional;

@SuppressWarnings("RedundantIfStatement")
@Service("swManager")
public class SWManagerImpl implements SWManager {

    @Autowired
    SWDao swDao;

    @Autowired
    SwIdPhotoDao swIdPhotoDao;

    @Autowired
    SwSurveyPhotoDao swSurveyPhotoDao;

    @Autowired
    SwUserBwmpMapDao swUserBwmpMapDao;

    @Autowired
    private LoanProductDao loanProductDao;

    @Autowired
    private BusinessTypeDao businessTypeDao;

    @Autowired
    private BusinessTypeAP3RDao businessTypeAp3rDao;

    @Autowired
    private ProductPlafondDao productPlafondDao;

    @Autowired
    private DirectBuyDao directBuyDao;

    @Autowired
    private AWGMDao awgmDao;

    @Autowired
    private NeighborDao neighborDao;

    @Autowired
    private SwProductMapDao swProductMapDao;

    @Autowired
    private OtherItemCalculationDao otherItemCalculationDao;

    @PersistenceContext
    public EntityManager em;

    @Override
    @Cacheable(value = "countAllSW", unless = "#result == null")
    public int countAll() {
        // TODO Auto-generated method stub
        return swDao.countAll();
    }

    @Override
    public Long findSwIdByLocalId(String swLocalId) {
        return swDao.findSWIdByLocalID(swLocalId);
    }

    @Override
    @Cacheable(value = "countAllSWByStatus", unless = "#result == null")
    public int countAllByStatus() {
        return swDao.countAllByStatus(WebGuiConstant.STATUS_WAITING_APPROVAL);
    }

    @Override
    public int countAllProduct() {
        // TODO Auto-generated method stub
        return loanProductDao.countAll();
    }

    @Override
    public int countAllBusinessType() {
        // TODO Auto-generated method stub
        return businessTypeDao.countAll();
    }

    @Override
    public Page<SurveyWawancara> findAll(List<String> kelIdList) {
        // TODO Auto-generated method stub
        return swDao.findByAreaIdInAndLocalIdIsNotNullAndIsDeleted(kelIdList, new PageRequest(0, countAll()), false);
    }

    @Override
    public Page<SurveyWawancara> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDate(kelIdList, calStart.getTime(), cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    public Boolean isValidSw(String swId) {
        Integer count = swDao.countByLocalId(swId);
        if (count > 0)
            return true;
        else
            return false;
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "countAllSW", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hawkeye.sw.countAllSW", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "countAllSWByStatus", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "isValidSw", key = "#surveyWawancara.swId", beforeInvocation = true),
            @CacheEvict(value = "isValidSwToDelete", key = "#surveyWawancara.swId", beforeInvocation = true), @CacheEvict(value = "findAllSW", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "findSWByCreatedDate"), @CacheEvict(value = "findSWByCreatedDatePageable", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "getSWById", key = "#surveyWawancara.swId", beforeInvocation = true),
            @CacheEvict(value = "wolverine.sw.findByLocalId", key = "#surveyWawancara.localId", beforeInvocation = true),
            @CacheEvict(value = "findAllProduct", allEntries = true),
            @CacheEvict(value = "hawkeye.sw.findByUserMSWithDate", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "hawkeye.sw.findIsDeletedSwList", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "findAllByUser", key = "#surveyWawancara.swId", beforeInvocation = true),
            @CacheEvict(value = "findAllByUserPageable", key = "#surveyWawancara.swId", beforeInvocation = true),
            @CacheEvict(value = "findByCreatedDateAndUser", key = "#surveyWawancara.swId", beforeInvocation = true),
            @CacheEvict(value = "findByCreatedDateAndUserPageable", key = "#surveyWawancara.swId", beforeInvocation = true),
            @CacheEvict(value = "getAllCustomerByLocPageable", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "getAllCustomerByLoc", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "getAllCustomerByLocAndCreatedDate", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "getAllCustomerByLocAndCreatedDatePagable", beforeInvocation = true, allEntries = true),
            @CacheEvict(value = "getSWByRrn", beforeInvocation = true, allEntries = true)})
    public void save(SurveyWawancara surveyWawancara) {
        // TODO Auto-generated method stub
        swDao.save(surveyWawancara);

    }

    @Override
    @Caching(evict = {@CacheEvict(value = "getSwIdPhoto", key = "#swIdPhoto.swId"),
            @CacheEvict(value = "getSwIdPhotoById", key = "#swIdPhoto.id", beforeInvocation = true),
            @CacheEvict(value = "hawkeye.swIdPhoto.getSwIdPhoto", allEntries = true, beforeInvocation = true)})
    public void save(SwIdPhoto swIdPhoto) {
        // TODO Auto-generated method stub
        swIdPhotoDao.save(swIdPhoto);

    }

    @Override
    @Caching(evict = {@CacheEvict(value = "getSwSurveyPhoto", key = "#SwSurveyPhoto.swId", beforeInvocation = true),
            @CacheEvict(value = "hawkeye.swSurveyPhoto.getSwSurveyPhoto", allEntries = true, beforeInvocation = true)})
    public void save(SwSurveyPhoto SwSurveyPhoto) {
        // TODO Auto-generated method stub
        swSurveyPhotoDao.save(SwSurveyPhoto);

    }

    @Override
    public SwIdPhoto getSwIdPhoto(String swId) {
        // TODO Auto-generated method stub
        return swIdPhotoDao.getSwIdPhoto(Long.parseLong(swId));
    }

    @Override
    public SwSurveyPhoto getSwSurveyPhoto(String swId) {
        // TODO Auto-generated method stub
        return swSurveyPhotoDao.getSwSurveyPhoto(Long.parseLong(swId));
    }

    @Override
    public SurveyWawancara getSWById(String swId) {
        // TODO Auto-generated method stub
        return swDao.findSwBySwId(Long.parseLong(swId));
    }

    @Override
    @CacheEvict(value = {"countAllSW", "countAllSWByStatus", "findAllSW", "findAllSWPageable", "findSWByCreatedDate",
            "findSWByCreatedDatePageable", "isValidSw", "hawkeye.swProductMapping.findProductMapBySwId", "isValidToDelete", "findByLocalId", "getSwIdPhoto", "hawkeye.sw.countAllSW",
            "hawkeye.otherItemCalculation.findOtherItemCalcBySwId", "hawkeye.swUserBwmpMapping.findSWUserBWMPMappingBySW",
            "getSWById", "getSWByRrn", "hawkeye.swIdPhoto.getSwIdPhoto", "hawkeye.swSurveyPhoto.getSwSurveyPhoto", "hawkeye.directBuyThings.findProductBySwId",
            "hawkeye.awgmBuyThings.findAwgmProductBySwId", "findAllByUser", "findAllByUserPageable", "findByCreatedDateAndUser", "hawkeye.sw.findByUserMSWithDate", "hawkeye.sw.findIsDeletedSwList",
            "hawkeye.neighborRecommendation.findNeighborBySwId", "findByCreatedDateAndUserPageable", "findProductMapByLocalId", "findDirectThingsBySwId", "findAwgmThingsBySwId", "findNeighborBySwId",
            "findSwMapApprovalBySwId", "getSwIdPhotoById", "getSwSurveyPhoto", "wolverine.sw.findByLocalId"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub

    }

    @Override
    public SurveyWawancara findByRrn(String rrn) {
        // TODO Auto-generated method stub
        return swDao.findByRrnAndLocalIdIsNotNull(rrn);
    }

    @Override
    public void save(DirectBuyThings directBuyProduct) {
        // TODO Auto-generated method stub
        directBuyDao.save(directBuyProduct);
    }

    @Override
    public void save(AWGMBuyThings awgmBuyProduct) {
        // TODO Auto-generated method stub
        awgmDao.save(awgmBuyProduct);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "findSwMapApprovalBySwId", key = "#swUserBWMPMapping.swId", beforeInvocation = true),
            @CacheEvict(value = "hawkeye.swUserBwmpMapping.findSWUserBWMPMappingBySW", key = "#swUserBWMPMapping.swId", beforeInvocation = true)
    })
    public void save(SWUserBWMPMapping swUserBWMPMapping) {
        // TODO Auto-generated method stub
        swUserBwmpMapDao.save(swUserBWMPMapping);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "findNeighborBySwId", key = "#neighbor.swId", beforeInvocation = true),
            @CacheEvict(value = "hawkeye.neighborRecommendation.findNeighborBySwId", allEntries = true, beforeInvocation = true)
    })
    public void save(NeighborRecommendation neighbor) {
        // TODO Auto-generated method stub
        neighborDao.save(neighbor);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "hawkeye.otherItemCalculation.findOtherItemCalcBySwId", key = "#otherItemCalculation.swId", beforeInvocation = true)
    })
    public void save(OtherItemCalculation otherItemCalculation) {
        otherItemCalculationDao.save(otherItemCalculation);
    }

    @Override
    public List<SWProductMapping> findProductMapBySwId(Long swId) {
        // TODO Auto-generated method stub
        return swProductMapDao.findSwProductMapBySwId(swId);
    }

    @Override
    public SWProductMapping findProductMapById(Long id) {
        return swProductMapDao.findByMappingId(id);
    }

    public void setParameter(Query q, Map<String, SurveyWawancara> parameterList) {
        for (String key : parameterList.keySet()) {
            q.setParameter(key, parameterList.get(key));
        }
    }

    @Override
    public List<DirectBuyThings> findProductBySwId(SurveyWawancara sw) {
        // TODO Auto-generated method stub
        return directBuyDao.findBySwId(sw);
    }

    @Override
    public List<AWGMBuyThings> findAwgmProductBySwId(SurveyWawancara sw) {
        // TODO Auto-generated method stub
        return awgmDao.findBySwId(sw);
    }

    @Override
    @Cacheable(value = "findNeighborBySwId", unless = "#result == null")
    public List<NeighborRecommendation> findNeighborBySwId(SurveyWawancara sw) {
        // TODO Auto-generated method stub
        return neighborDao.findBySwId(sw);
    }

    @Override
    public List<OtherItemCalculation> findOtherItemCalcBySwId(Long swId) {
        return otherItemCalculationDao.findBySwId(swId);
    }

    @Override
    @Cacheable(value = "findSwMapApprovalBySwId", unless = "#result == null")
    public List<SWUserBWMPMapping> findBySw(Long sw) {
        return swUserBwmpMapDao.findBySwId(sw);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "hawkeye.swProductMapping.findProductMapBySwId", key = "#map.swId", beforeInvocation = true)
    })
    public void save(SWProductMapping map) {
        // TODO Auto-generated method stub
        swProductMapDao.save(map);
    }

    @Override
    public LoanProduct findByProductId(String productId) {
        // TODO Auto-generated method stub
        return loanProductDao.findByProductId(Long.parseLong(productId));
    }

    @Override
    //@Cacheable(value = "wolverine.sw.findByLocalId", unless = "#result == null")
    public SurveyWawancara findByLocalId(String localId) {
        return swDao.findByLocalId(localId);
    }

    @Override
    public SWProductMapping findProductMapByLocalId(String localId) {
        return swProductMapDao.findByLocalId(localId);
    }

    @Override
    public List<SurveyWawancara> findIsDeletedSwList() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -7);
        Date dateBefore7Days = cal.getTime();
        return swDao.findIsDeletedSwId(dateBefore7Days, new Date());
    }

    @Override
    @Transactional
    public void deleteDirectBuy(DirectBuyThings id) {
        directBuyDao.delete(id);
    }

    @Override
    @Transactional
    public void deleteAwgmBuy(AWGMBuyThings id) {
        awgmDao.delete(id);
    }

    @Override
    @Transactional
    @Caching(evict = {@CacheEvict(value = "findNeighborBySwId", key = "#id.swId")
    })
    public void deleteReffNeighbour(NeighborRecommendation id) {
        neighborDao.delete(id);
    }

    @Override
    @Transactional
    public void deleteOtherItemCalc(OtherItemCalculation id) {
        otherItemCalculationDao.delete(id);
    }

    @Override
    public Page<SurveyWawancara> findAllByUser(List<String> userIdList) {
        // TODO Auto-generated method stub
        return swDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeleted(userIdList, new PageRequest(0, countAll()), false);
    }

    @Override
    public Page<SurveyWawancara> findAllByUserPageable(List<String> userIdList, PageRequest pageRequest) {
        // TODO Auto-generated method stub
        return swDao.findByCreatedByInAndLocalIdIsNotNullAndIsDeleted(userIdList, pageRequest, false);
    }

    @Override
    public Page<SurveyWawancara> findByCreatedDateAndUser(List<String> userIdList, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDateAndUser(userIdList, calStart.getTime(), cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
    }

    @Override
    public Page<SurveyWawancara> findByCreatedDateAndUserPageable(List<String> userIdList, Date startDate,
                                                                  Date endDate, PageRequest pageRequest) {
        // TODO Auto-generated method stub
        Calendar cal = Calendar.getInstance();
        Calendar calStart = Calendar.getInstance();
        cal.setTime(endDate);
        calStart.setTime(startDate);
        cal.add(Calendar.DATE, 1);
        calStart.add(Calendar.DATE, -7);
        return swDao.findByCreatedDateAndUser(userIdList, calStart.getTime(), cal.getTime(), pageRequest);
    }

    @Override
    @Cacheable(value = "findAllProduct", unless = "#result == null")
    public Page<LoanProduct> findAllProduct() {
        // TODO Auto-generated method stub
        return loanProductDao.findAll(new PageRequest(0, countAllProduct()));
    }

    @Override
    public Page<LoanProduct> findAllProductPageable(PageRequest pageRequest) {
        // TODO Auto-generated method stub
        return loanProductDao.findAll(pageRequest);
    }

    @Override
    public List<ProductPlafond> findByProductId(LoanProduct productId) {
        // TODO Auto-generated method stub
        return productPlafondDao.findByProductIdOrderByPlafond(productId);
    }

    @Override
    public BusinessTypeAP3R findBusinessTypeAP3RById(Long businessId) {
        // TODO Auto-generated method stub
        return businessTypeAp3rDao.findByBusinessId(businessId);
    }

    @Override
    public BusinessTypeAP3R findBySubBiCode(String subBiCode) {
        return businessTypeAp3rDao.findBySubBiCode(subBiCode);
    }
    
    /*
 	 * @author : Ilham
 	 * @Since  : 20191128
 	 * @category patch 
 	 *    - set data disbursmentdate sw
 	 */ 
	@Override
	public SurveyWawancara updatedisbursSw(SurveyWawancara surveyWawancara) {
		// TODO Auto-generated method stub
		return swDao.save(surveyWawancara);
	}

}
