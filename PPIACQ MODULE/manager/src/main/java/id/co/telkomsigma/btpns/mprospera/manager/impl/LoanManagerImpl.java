package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.dao.DeviationOfficerMapDao;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.loan.DeviationOfficerMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.dao.LoanDao;
import id.co.telkomsigma.btpns.mprospera.dao.LoanDeviationDao;
import id.co.telkomsigma.btpns.mprospera.dao.MailTokenDao;
import id.co.telkomsigma.btpns.mprospera.manager.LoanManager;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanDeviation;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailToken;

@SuppressWarnings("RedundantIfStatement")
@Service("loanManager")
public class LoanManagerImpl implements LoanManager {

    @Autowired
    private LoanDao loanDao;

    @Autowired
    private LoanDeviationDao loanDeviationDao;

    @Autowired
    private DeviationOfficerMapDao deviationOfficerMapDao;

    @Autowired
    private MailTokenDao mailTokenDao;

    @Override
    @Caching(evict = {
            @CacheEvict(value = "ppiacq.loan.findLoanByCreatedDate", allEntries = true),
            @CacheEvict(value = "ppiacq.loan.getLoanByRrn", allEntries = true),
            @CacheEvict(value = "ppiacq.loan.findByLoanId", allEntries = true)
    })
    public void save(Loan loan) {
        loanDao.save(loan);
    }

    @Override
    @Cacheable(value = "ppiacq.loan.findLoanByCreatedDate", unless = "#result == null")
    public Page<Loan> findByCreatedDate(Date startDate, Date endDate, String officeId) {
        // TODO Auto-generated method stub
        return loanDao.findByCreatedDate(startDate, endDate, new PageRequest(0, Integer.MAX_VALUE), officeId);
    }

    @Override
    @CacheEvict(value = {"ppiacq.loan.findLoanByCreatedDate", "ppiacq.loan.getLoanByRrn",
            "ppiacq.loan.findByLoanId"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub
    }

    @Override
    @Cacheable(value = "ppiacq.loan.findByLoanId", unless = "#result == null")
    public Loan findById(long parseLong) {
        // TODO Auto-generated method stub
        return loanDao.findOne(parseLong);
    }

    @Override
    @Cacheable(value = "ppiacq.loan.getLoanByRrn", unless = "#result == null")
    public Loan getLoanByRrn(String rrn) {
        // TODO Auto-generated method stub
        return loanDao.findByRrn(rrn);
    }

    @Override
    public Loan findByAp3rAndCustomer(Long ap3rId, Customer customer) {
        List<String> statusList = new ArrayList<>();
        statusList.add(WebGuiConstant.STATUS_ACTIVE);
        statusList.add(WebGuiConstant.STATUS_APPROVED);
        statusList.add(WebGuiConstant.STATUS_DRAFT);
        return loanDao.findTop1ByAp3rIdAndCustomerAndAp3rIdIsNotNullAndIsDeletedFalseAndStatusIn
                (ap3rId, customer, statusList);
    }

    @Override
    public Loan getLoanByAp3rId(Long ap3rId) {
        return loanDao.findByAp3rIdAndIsDeletedFalse(ap3rId);
    }

    @Override
    public LoanDeviation findByAp3rId(Long ap3rId, Boolean isDeleted) {
        return loanDeviationDao.findByAp3rIdAndIsDeleted(ap3rId, isDeleted);
    }

    @Override
    public int countDeviationWithStatus(Long id) {
        return deviationOfficerMapDao.countDeviation(id, WebGuiConstant.STATUS_WAITING);
    }

    @Override
    public int countDeviationWaiting(Long id) {
        return deviationOfficerMapDao.countDeviationWaiting(id, WebGuiConstant.STATUS_WAITING);
    }

    @Override
    public DeviationOfficerMapping findTopByDeviationIdAndStatus(Long deviationId) {
        return deviationOfficerMapDao.findTopByDeviationIdAndDateNotOrderByUpdatedDateDesc(deviationId, WebGuiConstant.STATUS_WAITING);
    }

    @Override
    public List<DeviationOfficerMapping> findByLoanDeviationId(String deviationId) {
        return deviationOfficerMapDao.findByDeviationId(Long.parseLong(deviationId));
    }

    @Override
    public int countLoanBySWId(Long swId) {
        return loanDao.countBySwId(swId);
    }

    @Override
    public void save(DeviationOfficerMapping deviationOfficerMapping) {
        deviationOfficerMapDao.save(deviationOfficerMapping);
    }

    @Override
    public void save(LoanDeviation loanDeviation) {
        loanDeviationDao.save(loanDeviation);
    }

    @Override
    public boolean countMailTokenSwId(String swId, String status) {
        // TODO Auto-generated method stub
        Integer count = mailTokenDao.countBySwId(swId, status);
        if (count > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public MailToken findBySwIdAndStatusAndModifiedByNotLikeAndModifiedByIsNotNull(String swId, String status, String modifiedby) {
        // TODO Auto-generated method stub
        return mailTokenDao.findBySwIdAndStatusAndModifiedByNotLikeAndModifiedByIsNotNull(swId, status, modifiedby);
    }

}