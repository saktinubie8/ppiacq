package id.co.telkomsigma.btpns.mprospera.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.manager.HolidayManager;

@Service("holidayService")
public class HolidayService  extends GenericService{
	@Autowired
    private HolidayManager holidayManager;
	  public int countHoliday(String tglAwal, String tglAkhir)  throws ParseException{
		    SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd");
		    Integer count = holidayManager.countHoliday(formatterDate.parse(tglAwal),formatterDate.parse(tglAkhir));
		    return count;
	    }
}
