package id.co.telkomsigma.btpns.mprospera.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.btpns.mprospera.model.user.Role;

/**
 * Created by daniel on 4/16/15.
 */
public interface RoleDao extends JpaRepository<Role, String> {

}