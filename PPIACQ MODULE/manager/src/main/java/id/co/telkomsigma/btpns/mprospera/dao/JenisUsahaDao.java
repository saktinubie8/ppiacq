package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.jenisusaha.JenisUsaha;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;


public interface JenisUsahaDao extends JpaRepository<JenisUsaha, Long>{
	JenisUsaha findByKdSectorEkonomi(Long kdSectorEkonomi);
	
	JenisUsaha findTop1ByCreatedDateOrderByIdDesc(Date date);
	
	JenisUsaha findTop1ByCreatedDateGreaterThanOrderByCreatedDateDesc(Date date);
	
	JenisUsaha findTop1ByOrderByCreatedDateDesc();
	
	List<JenisUsaha> findByOrderByCreatedDate();

}
