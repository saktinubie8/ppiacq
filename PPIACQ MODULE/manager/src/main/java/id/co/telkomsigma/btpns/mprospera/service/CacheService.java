package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("cacheService")
public class CacheService extends GenericService {

    @Autowired
    AreaManager areaManager;
    @Autowired
    CustomerManager customerManager;
    @Autowired
    LoanManager loanManager;
    @Autowired
    ParamManager paramManager;
    @Autowired
    RoleManager roleManager;
    @Autowired
    SentraManager sentraManager;
    @Autowired
    SWManager swManager;
    @Autowired
    TerminalManager terminalManager;
    @Autowired
    UserManager userManager;
    @Autowired
    LocationManager locationManager;

    public void clearCache() {
        areaManager.clearCache();
        customerManager.clearCache();
        loanManager.clearCache();
        paramManager.clearCache();
        roleManager.clearCache();
        sentraManager.clearCache();
        swManager.clearCache();
        terminalManager.clearCache();
        userManager.clearCache();
        locationManager.clearCache();
    }

}
