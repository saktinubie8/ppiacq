package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.user.Role;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.data.domain.Page;

/**
 * Created by daniel on 4/16/15.
 */
public interface RoleManager {
    /**
     * Get all roles
     *
     * @return list of Role object
     */

    List<Role> getAll();

    void clearCache();

}