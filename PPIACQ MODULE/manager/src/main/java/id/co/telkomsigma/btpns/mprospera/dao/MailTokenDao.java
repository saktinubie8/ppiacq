package id.co.telkomsigma.btpns.mprospera.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailToken;

public interface MailTokenDao extends JpaRepository<MailToken, Long> {

    @Query("SELECT COUNT(a) FROM MailToken a WHERE a.swId = :swId and a.status = :status")
    Integer countBySwId(@Param("swId") String swId, @Param("status") String status);

    MailToken findBySwIdAndStatusAndModifiedByNotLikeAndModifiedByIsNotNull(String swId, String status, String modifiedby);

}