package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.ppi.FamilyDataPPI;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FamilyDataPPIDao extends JpaRepository<FamilyDataPPI, Long> {

    FamilyDataPPI findByFamilyDataPpiLocalId(String familyLocalId);

    List<FamilyDataPPI> findByPpiIdAndDeletedFalse(Long ppiId);

}
