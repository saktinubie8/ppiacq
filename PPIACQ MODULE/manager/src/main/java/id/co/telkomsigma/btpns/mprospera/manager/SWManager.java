package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.sw.*;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface SWManager {

    int countAll();

    Long findSwIdByLocalId(String swLocalId);

    int countAllByStatus();

    int countAllProduct();

    int countAllBusinessType();

    Page<SurveyWawancara> findAll(List<String> kelIdList);

    Page<SurveyWawancara> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate);

    Boolean isValidSw(String swId);

    void save(SurveyWawancara surveyWawancara);

    void save(SwIdPhoto swIdPhoto);

    void save(SwSurveyPhoto swSurveyPhoto);

    SwIdPhoto getSwIdPhoto(String swId);

    SwSurveyPhoto getSwSurveyPhoto(String swId);

    SurveyWawancara getSWById(String swId);

    SurveyWawancara findByRrn(String rrn);
    
    SurveyWawancara updatedisbursSw(SurveyWawancara surveyWawancara);

    SurveyWawancara findByLocalId(String localId);

    LoanProduct findByProductId(String productId);

    List<ProductPlafond> findByProductId(LoanProduct productId);

    List<SWProductMapping> findProductMapBySwId(Long swId);

    SWProductMapping findProductMapById(Long id);

    List<DirectBuyThings> findProductBySwId(SurveyWawancara sw);

    List<AWGMBuyThings> findAwgmProductBySwId(SurveyWawancara sw);

    SWProductMapping findProductMapByLocalId(String localId);

    List<NeighborRecommendation> findNeighborBySwId(SurveyWawancara sw);

    List<OtherItemCalculation> findOtherItemCalcBySwId(Long swId);

    List<SWUserBWMPMapping> findBySw(Long sw);

    void save(DirectBuyThings directBuyProduct);

    void save(AWGMBuyThings awgmBuyProduct);

    void save(NeighborRecommendation neighbor);

    void save(OtherItemCalculation otherItemCalculation);

    void save(SWProductMapping map);

    void save(SWUserBWMPMapping map);

    void clearCache();

    List<SurveyWawancara> findIsDeletedSwList();

    void deleteDirectBuy(DirectBuyThings id);

    void deleteAwgmBuy(AWGMBuyThings id);

    void deleteReffNeighbour(NeighborRecommendation id);

    void deleteOtherItemCalc(OtherItemCalculation id);

    Page<SurveyWawancara> findAllByUser(List<String> userIdList);

    Page<SurveyWawancara> findAllByUserPageable(List<String> userIdList, PageRequest pageRequest);

    Page<SurveyWawancara> findByCreatedDateAndUser(List<String> userIdList, Date startDate, Date endDate);

    Page<SurveyWawancara> findByCreatedDateAndUserPageable(List<String> userIdList, Date startDate, Date endDate,
                                                           PageRequest pageRequest);

    Page<LoanProduct> findAllProduct();

    Page<LoanProduct> findAllProductPageable(PageRequest pageRequest);

    BusinessTypeAP3R findBusinessTypeAP3RById(Long businessId);

    BusinessTypeAP3R findBySubBiCode(String subBiCode);

}
