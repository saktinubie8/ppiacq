package id.co.telkomsigma.btpns.mprospera.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.dao.RoleDao;
import id.co.telkomsigma.btpns.mprospera.manager.RoleManager;
import id.co.telkomsigma.btpns.mprospera.model.user.Role;

import java.util.List;

/**
 * Created by daniel on 4/16/15.
 */
@Service("roleManager")
public class RoleManagerImpl implements RoleManager {

    @Autowired
    private RoleDao roleDao;

    @Override
    @Cacheable(value = "ppiacq.role.allRoles", unless = "#result == null")
    public List<Role> getAll() {
        return roleDao.findAll();
    }

    @Override
    @CacheEvict(value = {"roleById", "allRoles", "userByRoleId",
            "userByRoleName", "ppiacq.role.allRoles"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {

    }

}
