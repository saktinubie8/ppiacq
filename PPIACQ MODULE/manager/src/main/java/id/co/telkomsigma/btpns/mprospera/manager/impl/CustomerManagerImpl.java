package id.co.telkomsigma.btpns.mprospera.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.dao.CustomerDao;
import id.co.telkomsigma.btpns.mprospera.manager.CustomerManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;

@SuppressWarnings("RedundantIfStatement")
@Service("customerManager")
public class CustomerManagerImpl implements CustomerManager {

    @Autowired
    private CustomerDao customerDao;

    @Override
    @Cacheable(value = "ppiacq.customer.countCustomerByUsername", unless = "#result == null")
    public Integer countCustomerByUsername(String assignedUsername) {
        return customerDao.countByAssignedUsername(assignedUsername);
    }

    @Override
    @Caching(evict = {
            @CacheEvict(value = "ppiacq.customer.countCustomerByUsername", allEntries = true),
            @CacheEvict(value = "ppiacq.customer.getBySwId", allEntries = true)
    })
    public void save(Customer customer) {
        // TODO Auto-generated method stub
        customerDao.save(customer);
    }

    @Override
    @CacheEvict(allEntries = true, value = {"ppiacq.customer.countCustomerByUsername",
            "ppiacq.customer.getBySwId"}, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub
    }

    @Override
    @Cacheable(value = "ppiacq.customer.getBySwId", unless = "#result == null")
    public Customer getBySwId(Long swId) {
        // TODO Auto-generated method stub
        return customerDao.findTopBySwId(swId);
    }

    @Override
    public Customer findById(long parseLong) {
        // TODO Auto-generated method stub
        return customerDao.findOne(parseLong);
    }

}