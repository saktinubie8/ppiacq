package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.LinkedHashMap;

import id.co.telkomsigma.btpns.mprospera.dao.TerminalActivityDao;
import id.co.telkomsigma.btpns.mprospera.dao.TerminalDao;
import id.co.telkomsigma.btpns.mprospera.manager.TerminalManager;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 * Created by Dzulfiqar on 11/11/15.
 */
@Service("terminalManager")
public class TerminalManagerImpl implements TerminalManager {

    @Autowired
    TerminalDao terminalDao;

    @Autowired
    TerminalActivityDao terminalActivityDao;

    @Cacheable(value = "ppiacq.terminal.terminalByImei", unless = "#result == null")
    public Terminal getTerminalByImei(String imei) {
        return terminalDao.findByImei(imei);
    }

    @Override
    @Caching(evict = {@CacheEvict("allTerminal")})
    public Terminal updateTerminal(Terminal terminal) {
        terminalDao.save(terminal);
        return terminal;
    }

    @Override
    @CacheEvict(value = {"terminalByImei", "allTerminal", "ppiacq.terminal.terminalByImei"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {
        // TODO Auto-generated method stub

    }

}
