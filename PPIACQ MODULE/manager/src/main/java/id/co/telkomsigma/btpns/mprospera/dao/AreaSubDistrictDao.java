package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.List;

import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrict;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AreaSubDistrictDao extends JpaRepository<AreaSubDistrict, String> {

    List<AreaSubDistrict> findByParentAreaId(String areaId);

}
