package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.jenisusaha.JenisUsaha;

import java.util.Date;
import java.util.List;


public interface JenisUsahaManager {

	JenisUsaha findById(Long id);
	JenisUsaha findByKdSectorEkonomi(Long kdSectorEkonomi);
	JenisUsaha doSave(JenisUsaha jenisUsaha);
	List<JenisUsaha> getAllJenisUsaha();
	
	JenisUsaha getJenisUsahaByDate(Date date);
	
	JenisUsaha findTop1ByCreateDateMoreThanOrderByCreatedDateDesc(Date date);
	
	JenisUsaha findTop1ByCreateDateOrderByCreatedDateDesc();
}
