package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.parameter.Holiday;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by fujitsuPC on 5/16/2017.
 */
public interface HolidayDao extends JpaRepository<Holiday, Long> {
	
    @Query(value = "select COUNT(holiday_dt) from t_holiday \r\n" + 
    		       "where CONVERT (date, holiday_dt) "+
    		       "BETWEEN :tglAwal and :tglAkhir ", nativeQuery = true)
    Integer getHoliday(@Param("tglAwal") Date tglAwal, @Param("tglAkhir") Date tglAkhir);
}