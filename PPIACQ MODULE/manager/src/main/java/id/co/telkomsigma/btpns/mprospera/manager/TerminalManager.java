package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import org.springframework.data.domain.Page;

import java.util.LinkedHashMap;

public interface TerminalManager {

    Terminal getTerminalByImei(String imei);

    Terminal updateTerminal(Terminal terminal);

    void clearCache();

}
