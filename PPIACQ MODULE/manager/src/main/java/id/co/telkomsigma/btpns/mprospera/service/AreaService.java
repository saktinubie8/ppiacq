package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.AreaManager;
import id.co.telkomsigma.btpns.mprospera.manager.LocationManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaDistrict;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service("areaService")
public class AreaService extends GenericService {

    @Autowired
    private AreaManager areaManager;

    @Autowired
    private LocationManager locationManager;

    public Integer countAll(AreaDistrict areaDistrictByUser) {
        return areaManager.getAllSubDistrictDetailsByParentAreaId(areaDistrictByUser.getAreaId()).getSize();
    }

    public Location findLocationById(String id) {
        return locationManager.findByLocationId(id);
    }

}
