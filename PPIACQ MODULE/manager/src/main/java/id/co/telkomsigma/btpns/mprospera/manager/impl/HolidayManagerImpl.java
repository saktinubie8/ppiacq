package id.co.telkomsigma.btpns.mprospera.manager.impl;


import java.util.Date;

import id.co.telkomsigma.btpns.mprospera.dao.HolidayDao;
import id.co.telkomsigma.btpns.mprospera.manager.HolidayManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("holidayManager")
public class HolidayManagerImpl implements HolidayManager {

    @Autowired
    private HolidayDao dao;

	@Override
	public int countHoliday(Date tglAwal, Date tglAkhir) {
		return dao.getHoliday(tglAwal, tglAkhir);
	}

}