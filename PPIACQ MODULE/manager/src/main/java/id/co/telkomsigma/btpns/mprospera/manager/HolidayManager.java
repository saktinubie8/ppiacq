package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.Date;

public interface HolidayManager {
    
    int countHoliday(Date tglAwal, Date tglAkhir);
}