package id.co.telkomsigma.btpns.mprospera.dao;


import id.co.telkomsigma.btpns.mprospera.model.sda.AreaKelurahan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AreaKelurahanDao extends JpaRepository<AreaKelurahan, String> {

}
