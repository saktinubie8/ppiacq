package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.btpns.mprospera.model.sw.AP3R;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AP3RDao extends JpaRepository<AP3R, Long> {

    @Query("SELECT COUNT(m) FROM AP3R m WHERE m.localId is not null AND m.isDeleted = false")
    int countAll();

    List<AP3R> findBySwIdAndIsDeleted(Long sw, Boolean deleted);

    @Query("SELECT m FROM AP3R m WHERE m.createdDate>=:startDate AND m.createdDate<:endDate AND m.createdBy in :userList AND m.localId is not null AND m.isDeleted = false ORDER BY m.createdBy ASC")
    Page<AP3R> findByCreatedDateAndUser(@Param("userList") List<String> userIdList,
                                        @Param("startDate") Date startDate, @Param("endDate") Date endDate, Pageable pageable);
    
    
    AP3R findByLocalId(String localId);
}
