package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.UserDao;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by daniel on 3/31/15.
 */
@Service("userManager")
public class UserManagerImpl implements UserManager {
    @Autowired
    private UserDao userDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    //@Cacheable(value = "ppi.user.userByUsername", unless = "#result == null")
    public User getUserByUsername(String username) {
        return userDao.findByUsername(username.toLowerCase());
    }

    @Override
    public User getUserByUserId(Long userId) {
        User user = userDao.findByUserId(userId);
        return user;
    }

    @Override
   // @Cacheable(value = "ppi.user.sessionKeyByUsername", unless = "#result == null")
    public String getSessionKeyByUsername(String username) {
        return userDao.findSessionKeyByUsername(username.toLowerCase());
    }

    @Override
    @CacheEvict(value = {"ppi.user.userByUsername", "ppi.user.userByLocId", "ppi.user.sessionKeyByUsername"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {

    }

    @Override
   // @Cacheable(value = "ppi.user.userByLocId", unless = "#result == null")
    public List<User> getUserByLocId(String locId) {
        // TODO Auto-generated method stub
        List<User> userWisma = userDao.findByOfficeCode(locId);
        return userWisma;
    }

    @Bean(name = "passwordEncoder")
    public PasswordEncoder getPasswordEncoder() {
        if (passwordEncoder == null) {
            passwordEncoder = new BCryptPasswordEncoder();
        }
        return passwordEncoder;
    }
}
